package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardListView;


public class AddPeopleActivity extends Activity {
    ContactsBroadcastReceiver contactsBroadcastReceiver;
    public static String memberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_people);
        memberList="";
        contactsBroadcastReceiver=new ContactsBroadcastReceiver();
        friendCardList();


        final CircleButton createEventButton=(CircleButton) findViewById(R.id.createEventButton);
        createEventButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "Event created");
                if(memberList.equals("")) Toast.makeText(AddPeopleActivity.this,"You need at least one other person to meet. :-P",Toast.LENGTH_SHORT).show();
                else {
                    Bundle extras = getIntent().getExtras();
                    extras.putString("memberList", memberList+extras.getString("creator"));
                    extras.putString("url", "/event/create");
                    //Intent eventCreatedIntent = new Intent(AddPeopleActivity.this, EventsActivity.class);
                    new GcmSender(extras,AddPeopleActivity.this).execute(null,null,null);
                    //startActivity(eventCreatedIntent);
                    try {
                        if (contactsBroadcastReceiver != null) {
                            AddPeopleActivity.this.unregisterReceiver(contactsBroadcastReceiver);
                            Log.i("freakout","contactsreceiver unregistered.");
                        }
                    } catch(Exception e) {e.printStackTrace();}

                    AddPeopleActivity.this.finish();
                }
            }

        });
        Bundle data = new Bundle();
        data.putString("url", "/user/getfriendlist");
        data.putString("phoneNumber", getPhNum(AddPeopleActivity.this));
        new GcmSender(data, AddPeopleActivity.this).execute(null, null, null);
        AddPeopleActivity.this.registerReceiver(contactsBroadcastReceiver,new IntentFilter("refreshContactsMsg"));

        final CircleButton refreshContactsButton=(CircleButton) findViewById(R.id.refreshContactsButton);
        refreshContactsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "Refresh contacts");
                ContactsInitializer ci=new ContactsInitializer();
                ci.initializeContacts(getApplicationContext(),getPhNum(getApplicationContext()));

                Bundle data = new Bundle();
                data.putString("url", "/user/getfriendlist");
                data.putString("phoneNumber", getPhNum(AddPeopleActivity.this));
                new GcmSender(data, AddPeopleActivity.this).execute(null, null, null);
            }

        });


    }

    private class ContactsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout","contacts Broadcast received");

            try { friendCardList(); } catch(Exception e) {e.printStackTrace();}


            //ExperimentFragment.textView2.setText(intent.getExtras().get("eventList").toString());
        }
    }

    private String getPhNum(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum=prefs.getString(MainActivity.PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");
            return "";
        }
        //TabbedActivity.this.phonePrefix=prefs.getString(PHONE_PREFIX,"");
        return phNum;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }


    //Function to create Contact List
    public void friendCardList() {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */
        SharedPreferences contactsCheck=getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        String contactList=contactsCheck.getString("contactList","");
        if(!(contactList.equals(""))) {
            Log.i("freakout",contactList);
            try {
                JSONArray contactsArray = new JSONArray(contactList);

                ArrayList<Card> cards = new ArrayList<Card>();

                for (int i = 0; i <contactsArray.length(); i++) {
                    // Create a Card
                    JSONObject contact=contactsArray.getJSONObject(i);
                    FriendCard card = new FriendCard(this, contact.getString("name"), contact.getString("phoneNumber"));

                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(this, cards);


                CardListView listView = (CardListView) this.findViewById(R.id.friendList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            } catch (Exception e) {e.printStackTrace();}
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        contactsBroadcastReceiver=new ContactsBroadcastReceiver();
        AddPeopleActivity.this.registerReceiver(contactsBroadcastReceiver, new IntentFilter("refreshContactsMsg"));

    }




    @Override
    public void onStop() {

        super.onStop();
        try {
            if (contactsBroadcastReceiver != null) {
                AddPeopleActivity.this.unregisterReceiver(contactsBroadcastReceiver);
                Log.i("freakout","contactsreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();
        try {
            if (contactsBroadcastReceiver != null) {
                AddPeopleActivity.this.unregisterReceiver(contactsBroadcastReceiver);
                Log.i("freakout","contactsreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
            if (contactsBroadcastReceiver != null) {
                AddPeopleActivity.this.unregisterReceiver(contactsBroadcastReceiver);
                Log.i("freakout","contactsreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_people, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}



//
//package com.heinz.freakoders.catchup;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//
//
//public class AddPeopleActivity extends Activity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_people);
//
//        final Button createEventButton=(Button) findViewById(R.id.createEventButton);
//        createEventButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                Log.i("freakout", "Event created");
//
//
//                Bundle extras= getIntent().getExtras();
//                extras.putString("memberList","+918956665474");
//                extras.putString("url","/event/create");
//                //Intent eventCreatedIntent = new Intent(AddPeopleActivity.this, EventsActivity.class);
//                //new GcmSender(extras,AddPeopleActivity.this).execute(null,null,null);
//                //startActivity(eventCreatedIntent);
//                AddPeopleActivity.this.finish();
//
//            }
//
//        });
//
//
//
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_add_people, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//}
