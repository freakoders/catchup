package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.support.v4.app.*;
import android.widget.Toast;

import com.doomonafireball.betterpickers.calendardatepicker.CalendarDatePickerDialog;
import com.doomonafireball.betterpickers.datepicker.DatePickerBuilder;
import com.doomonafireball.betterpickers.datepicker.DatePickerDialogFragment;
import com.doomonafireball.betterpickers.radialtimepicker.RadialTimePickerDialog;
import com.doomonafireball.betterpickers.timepicker.TimePickerBuilder;
import com.doomonafireball.betterpickers.timepicker.TimePickerDialogFragment;
import com.heinz.freakoders.catchup.MainActivity;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import at.markushi.ui.CircleButton;


public class AddSuggestionActivity extends FragmentActivity implements RadialTimePickerDialog.OnTimeSetListener,CalendarDatePickerDialog.OnDateSetListener {


    //for foursquare
    ArrayList venuesList;
    // the foursquare client_id and the client_secret
    final String CLIENT_ID = "GRLBDFSWN3C0GKVEJ1HI301TSOKMW2VIMCVAKCUENH1NQV2V";
    final String CLIENT_SECRET = "I3LZXCINYFWX5DNIYFUNLDAMTJFR1SYFEXTYW1AAOU4QGBV3";
    // we will need to take the latitude and the logntitude from a certain point
    // this is the center of New York
    final String latitude = "18.487354";
    final String longitude = "73.8206584";
    String queryString;


    //Test url
    // https://api.foursquare.com/v2/venues/suggestcompletion?client_id=GRLBDFSWN3C0GKVEJ1HI301TSOKMW2VIMCVAKCUENH1NQV2V&client_secret=I3LZXCINYFWX5DNIYFUNLDAMTJFR1SYFEXTYW1AAOU4QGBV3&v=20130815%20&ll=18.487354,73.8206584&query=vai


    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    private String[] monthLetters={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    public static final String USER_NAME = "displayName";
    public static final String PHONE_NUM="phoneNumber";
    public static final String PHONE_PREFIX="countryCode";
    public CaldroidFragment dialogCaldroidFragment;
    private String serverDate;
    private String serverTime;
    private SuggestionSuggestionsBroadcastReceiver suggestionSuggestionsReceiver;
    String activityRole;
    JSONArray suggestionArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_suggestion);
        activityRole=getIntent().getStringExtra("activityRole");
        suggestionSuggestionsReceiver=new SuggestionSuggestionsBroadcastReceiver();
        AddSuggestionActivity.this.registerReceiver(suggestionSuggestionsReceiver, new IntentFilter("suggestionsMsg"));
        TextView date=(TextView)findViewById(R.id.textSuggestionDatePicker);
        TextView time=(TextView)findViewById(R.id.textSuggestionTimePicker);
        date.setText(getIntent().getStringExtra("date"));
        time.setText(getIntent().getStringExtra("time"));
        serverDate=getIntent().getStringExtra("date");
        serverTime=getIntent().getStringExtra("time");
        CircleButton timePicker=(CircleButton)findViewById(R.id.suggestionTimePicker);
        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TimePickerBuilder tpb = new TimePickerBuilder()
//                        .setFragmentManager(getSupportFragmentManager())
//                        .setStyleResId(R.style.BetterPickersDialogFragment);
//                tpb.show();

                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int minute=cal.get(Calendar.MINUTE);
                RadialTimePickerDialog timePickerDialog = RadialTimePickerDialog
                        .newInstance(AddSuggestionActivity.this, hour, minute, true);
                timePickerDialog.setThemeDark(false);
                timePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);



            }
        });





        CircleButton datePicker = (CircleButton) findViewById(R.id.suggestionDatePicker);
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                CaldroidFragment caldroidFragment = new CaldroidFragment();
                Bundle args = new Bundle();
                Calendar cal = Calendar.getInstance();
                args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
                args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
                caldroidFragment.setArguments(args);

                android.support.v4.app.FragmentTransaction t = getSupportFragmentManager().beginTransaction();
                t.replace(R.id.cal, caldroidFragment);
                t.commit();
                */

                Calendar cal=Calendar.getInstance();
                int month=cal.get(Calendar.MONTH)+1;
                int year=cal.get(Calendar.YEAR);
                int day=cal.get(Calendar.DAY_OF_MONTH);
//                dialogCaldroidFragment = CaldroidFragment.newInstance("Select a date", month, year);
//                dialogCaldroidFragment.setCaldroidListener(caldroidListener);
//                dialogCaldroidFragment.show(getSupportFragmentManager(),"TAG");

                FragmentManager fm = getSupportFragmentManager();

                CalendarDatePickerDialog calendarDatePickerDialog = CalendarDatePickerDialog
                        .newInstance(AddSuggestionActivity.this,year, month - 1,
                                day);
                calendarDatePickerDialog.show(fm, FRAG_TAG_DATE_PICKER);

            }
        });

        final CircleButton addPeopleButton=(CircleButton) findViewById(R.id.addSuggestionButton);
        addPeopleButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "Add Suggestion clicked");


                AutoCompleteTextView suggestSuggestionVenue=(AutoCompleteTextView)findViewById(R.id.suggestSuggestionVenue);


                Bundle data=new Bundle();


                String ourVenue=suggestSuggestionVenue.getText().toString();
                if(!ourVenue.equals("")) {
                    data.putString("venue", ourVenue);
                    data.putString("venueLat", "");
                    data.putString("venueLon", "");
                    data.putString("venueAddress", "");
                    if(suggestionArray!=null) {

                        for (int i = 0; i < suggestionArray.length(); i++) {
                            try {
                                JSONObject suggestion = suggestionArray.getJSONObject(i);
                                if (suggestion.getString("name").equals(ourVenue)) {
                                    suggestion = suggestionArray.getJSONObject(i).getJSONObject("location");
                                    if(suggestion.has("lat")) data.putString("venueLat", suggestion.getString("lat"));
                                    if(suggestion.has("lng")) data.putString("venueLon", suggestion.getString("lng"));
                                    if(suggestion.has("address")) data.putString("venueAddress", suggestion.getString("address"));

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else {
                    Log.i("freakout","***************** " +"Lat "+getIntent().getStringExtra("venueLat")+" ****************");
                    data.putString("venue",getIntent().getStringExtra("venue"));
                    data.putString("venueLat",getIntent().getStringExtra("venueLat"));
                    data.putString("venueLon", getIntent().getStringExtra("venueLon"));
                    data.putString("venueAddress", getIntent().getStringExtra("venueAddress"));

                }
                data.putString("date",serverDate);
                data.putString("time",serverTime);
                //       addPeopleIntent.putExtra("date",Integer.toString(newEventDate.getDayOfMonth())+"-"+newEventDate.getMonth()+"-"+newEventDate.getYear());
                //       addPeopleIntent.putExtra("time",newEventTime.getCurrentHour().toString()+":"+newEventTime.getCurrentMinute().toString());
                data.putString("phoneNumber",new PreferencesAccesser(AddSuggestionActivity.this).getPhNum());
                if(activityRole.equals("suggestion")) data.putString("url","/event/addsuggestion");
                else if(activityRole.equals("editEvent")) data.putString("url","/event/edit");
                data.putString("id",getIntent().getStringExtra("id"));
                new GcmSender(data,AddSuggestionActivity.this).execute(null,null,null);


                AddSuggestionActivity.this.finish();

            }

        });



        //Creating the instance of ArrayAdapter containing list of language names

        //Getting the instance of AutoCompleteTextView
        final AutoCompleteTextView suggestSuggestionVenue= (AutoCompleteTextView)findViewById(R.id.suggestSuggestionVenue);

        suggestSuggestionVenue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                Log.i("freakout","the textChanged count is "+suggestSuggestionVenue.length());
//                List<String> venuee= new ArrayList<String>();

//                SecureRandom random=new SecureRandom();
//                for(int i=0;i<3;i++)
//                {
//                    String temp= "siddharth"+new BigInteger(130,random).toString(32);
//                    venuee.add(temp);
//
//                }

//                if(suggestSuggestionVenue.length()>2) {
//                    Bundle data = new Bundle();
//                    data.putString("url", "/user/getplacesuggestion");
//                    data.putString("latitude", "18.43");
//                    data.putString("longitude", "73.66");
//                    data.putString("queryString", "" + suggestSuggestionVenue.getText());
//                    new GcmSender(data, AddSuggestionActivity.this).execute(null, null, null);
//                    Log.i("freakout", "sent the querry: " + suggestSuggestionVenue.getText().toString());
//
//
//
//                }



//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.select_dialog_item, venuee);
//
//
//                suggestVenue.setAdapter(adapter);
//                suggestVenue.setThreshold(1);//will start working from first character
//                suggestVenue.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
//                suggestVenue.setTextColor(Color.BLACK);
//                suggestVenue.setDropDownBackgroundResource(R.color.catchup_dark);
//
                if(s.length()>2 && s.length()<6) {
                    Log.i("freakout","foursquare request sent at s.length() = "+s.length()+"  "+s);
                    queryString=s.toString();
                    new foursquare().execute();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private String getPhNum(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum=prefs.getString(PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");
            return "";
        }

        //String phonePrefix=prefs.getString(PHONE_PREFIX,"");

        return phNum;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_suggestion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    final CaldroidListener caldroidListener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
            //Toast.makeText(getApplicationContext(), date.toString(),
            //        Toast.LENGTH_SHORT).show();

            dialogCaldroidFragment.dismiss();


            TextView datePicker = (TextView) findViewById(R.id.textSuggestionDatePicker);
            SimpleDateFormat formatter=new SimpleDateFormat("MMM dd, yyyy");

            datePicker.setText(formatter.format(date));
            formatter=new SimpleDateFormat("dd-MMM-yyyy");
            serverDate=formatter.format(date);

        }



        @Override
        public void onChangeMonth(int month, int year) {
        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {


            Button leftButton = dialogCaldroidFragment.getLeftArrowButton();
            leftButton.setTextColor(getResources().getColor(R.color.catchup_accent));

            Button rightButton = dialogCaldroidFragment.getRightArrowButton();
            rightButton.setTextColor(getResources().getColor(R.color.catchup_accent));

        }

    };


//    @Override
//    public void onDialogTimeSet(int i, int i2, int i3) {
//
//        String ampm="AM";
//        TextView TexttimePicker=(TextView)findViewById(R.id.textSuggestionTimePicker);
//        String min="";
//        if(i3<10)
//            min="0";
//        serverTime=Integer.toString(i2)+":"+min+Integer.toString(i3);
//        if(i2>11)
//        {
//            i2=i2%12;
//            ampm="PM";
//
//        }
//        if(i2==0)
//        {
//            i2=12;
//        }
//
//        TexttimePicker.setText(""+i2+":"+i3+ampm);
//    }

    @Override
    public void onResume() {
        super.onResume();
        suggestionSuggestionsReceiver=new SuggestionSuggestionsBroadcastReceiver();
        AddSuggestionActivity.this.registerReceiver(suggestionSuggestionsReceiver, new IntentFilter("suggestionsMsg"));
    }

    @Override
    public void onStop() {

        super.onStop();
        try {
            if (suggestionSuggestionsReceiver != null)
                AddSuggestionActivity.this.unregisterReceiver(suggestionSuggestionsReceiver);

        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();
        try {
            if (suggestionSuggestionsReceiver != null)
                AddSuggestionActivity.this.unregisterReceiver(suggestionSuggestionsReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
            if (suggestionSuggestionsReceiver != null)
                AddSuggestionActivity.this.unregisterReceiver(suggestionSuggestionsReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public void onDateSet(CalendarDatePickerDialog calendarDatePickerDialog, int year, int month, int day) {
        TextView datePicker = (TextView) findViewById(R.id.textSuggestionDatePicker);
        SimpleDateFormat formatter=new SimpleDateFormat("MMM dd, yyyy");
        if(day<10) {
            datePicker.setText("" + monthLetters[month] + " 0" + day + ", " + year);
            serverDate="0"+day+"-"+monthLetters[month]+"-"+year;
        }else{
            datePicker.setText(""+monthLetters[month]+" "+day+", "+year);
            serverDate=""+day+"-"+monthLetters[month]+"-"+year;
        }

    }

    @Override
    public void onTimeSet(RadialTimePickerDialog radialTimePickerDialog, int i2, int i3) {
        TextView TexttimePicker=(TextView)findViewById(R.id.textSuggestionTimePicker);
        if(i3<10) {
            TexttimePicker.setText("" + i2 + ":0" + i3);
            serverTime="" + i2 + ":0" + i3;
        }
        else {
            TexttimePicker.setText("" + i2 + ":" + i3);
            serverTime="" + i2 + ":" + i3;
        }
    }

    private class SuggestionSuggestionsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout","Suggestions Broadcast received");
            List<String> venuee= new ArrayList<String>();
            try {
                suggestionArray = new JSONArray(intent.getStringExtra("miniVenues"));

                for(int i=0;i<suggestionArray.length();i++) {
                    JSONObject suggestion=suggestionArray.getJSONObject(i);
                    String suggestionString=suggestion.getString("name")+", "+suggestion.getString("address");
                    venuee.add(suggestionString);
                }
            } catch(Exception e) {e.printStackTrace();}
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.select_dialog_item, venuee);

            AutoCompleteTextView suggestVenue=(AutoCompleteTextView)findViewById(R.id.suggestSuggestionVenue);

            suggestVenue.setAdapter(adapter);
            suggestVenue.setThreshold(1);//will start working from first character
            suggestVenue.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
            suggestVenue.setTextColor(Color.BLACK);
            suggestVenue.setDropDownBackgroundResource(R.color.catchup_dark);



        }
    }


    private class foursquare extends AsyncTask<Void,Void,String> {

        String temp;

//        @Override
//        protected String doInBackground(View... urls) {
//            // make Call to the url
//            temp = makeCall("https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=40.7463956,-73.9852992");
//            return "";
//        }


        @Override
        protected String doInBackground(Void[] params) {
            // make Call to the url
            //temp = makeCall("https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=40.7463956,-73.9852992");
            temp = makeCall("https://api.foursquare.com/v2/venues/suggestcompletion?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll="+latitude+","+longitude+"&query="+queryString);

            return null;
        }

        @Override
        protected void onPreExecute() {
            // we can start a progress bar here
        }

        @Override
        protected void onPostExecute(String result) {
            if (temp == null) {
                // we have an error to the call
                // we can also stop the progress bar
                Log.i("freakout","venuesList :Empty");
            } else {
                // all things went right
                Log.i("freakout","temp: "+temp);
                // parseFoursquare venues search result
                venuesList = (ArrayList) parseFoursquare(temp);
                Log.i("freakout","venuesList :"+venuesList);

                //List<String> venuee= new ArrayList<String>();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.select_dialog_item, venuesList);
                AutoCompleteTextView suggestVenue=(AutoCompleteTextView)findViewById(R.id.suggestSuggestionVenue);
                suggestVenue.setAdapter(adapter);
                suggestVenue.setThreshold(3);//will start working from first character
                suggestVenue.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                suggestVenue.setTextColor(Color.BLACK);
                suggestVenue.setDropDownBackgroundResource(R.color.catchup_dark);

            }
        }
    }

    public static String makeCall(String url) {

        // string buffers the url
        StringBuffer buffer_string = new StringBuffer(url);
        String replyString = "";

        // instanciate an HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        // instanciate an HttpGet
        HttpGet httpget = new HttpGet(buffer_string.toString());

        try {
            // get the responce of the httpclient execution of the url
            HttpResponse response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();

            // buffer input stream the result
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            // the result as a string is ready for parsing
            replyString = new String(baf.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // trim the whitespaces
        return replyString.trim();
    }


    private ArrayList parseFoursquare(final String response) {

        ArrayList temp = new ArrayList();
        try {

            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);

            // make an jsonObject in order to parse the response
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("minivenues")) {
                    suggestionArray = jsonObject.getJSONObject("response").getJSONArray("minivenues");

                    for (int i = 0; i<suggestionArray.length();i++)
                    {
//                        if (suggestionArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).has("icon"))
//                        {
//                            poi.setCategory(suggestionArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).getString("name"));
//                        }

                        temp.add(suggestionArray.getJSONObject(i).getString("name"));

                    }
                }
                //temp.add(poi);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return new ArrayList();

        }
        return temp;
    }




}

