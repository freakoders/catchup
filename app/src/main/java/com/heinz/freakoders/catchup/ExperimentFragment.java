package com.heinz.freakoders.catchup;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.BreakIterator;
import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.CardThumbnail;
import it.gmariotti.cardslib.library.view.CardListView;
import it.gmariotti.cardslib.library.view.CardView;


public class ExperimentFragment extends Fragment implements View.OnClickListener {
    public static final String PHONE_NUM="phoneNumber";
    public static TextView textView2;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String eventList;
    //private String mParam2;

    //private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ExperimentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExperimentFragment newInstance(String param1) {
        Log.i("freakout","newInstance");
        ExperimentFragment fragment = new ExperimentFragment();
        Bundle args = new Bundle();
        args.putString("eventList", param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if (getArguments() != null) {
        /*
        eventList = getArguments().getString("eventList");
//        textView2=(TextView)getActivity().findViewById(R.id.textView2);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        //}
        Log.i("freakout","oncreate");
        //String eventList=getIntent().getStringExtra("eventList");
        //TextView allEventsView = (TextView) getActivity().findViewById(R.id.allEventsView);
        if(eventList==null) {
            Log.i("freakout","eventList==null");
            Bundle data = new Bundle();
            data.putString("url", "/user/getevents");
            data.putString("phoneNumber", getPhNum(getActivity().getApplicationContext()));
            String id = Integer.toString(1);
            new GcmSender(data, getActivity().getApplicationContext()).execute(null, null, null);
        }
        else {
            Log.i("freakout","eventList!=null \n"+ eventList);
            //allEventsView.setText(eventList);
            Log.i("freakout","eventList!=null done");
        }
        */



    }




    @Override
    public void onStart() {
        super.onStart();
        // demoCard();
        eventCardList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("freakout","entered onCreateView");
        if (container==null) return null;


        //eventCardList();



        FrameLayout mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_experiment,
                container, false);
        Log.i("freakout","inflated");

        // note that we're looking for a button with id="@+id/myButton" in your inflated layout
        // Naturally, this can be any View; it doesn't have to be a button

        CircleButton newEventButton = (CircleButton) mFrameLayout.findViewById(R.id.newEventButton);
        Log.i("freakout","newEventButton created");

        newEventButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "New Event clicked");

                Intent newEventIntent = new Intent(getActivity(), NewEventActivity.class);
                startActivity(newEventIntent);
                //EventsActivity.this.finish();

            }

        });

        Log.i("freakout", "newEventButton listener set");


        // Inflate the layout for this fragment
        return mFrameLayout;
    }

    @Override
    public void onClick(View v) {
        Log.i("freakout", "New Event clicked");

        //Intent newEventIntent = new Intent(EventsActivity.this, NewEventActivity.class);
        //startActivity(newEventIntent);
        //EventsActivity.this.finish();


    }
    /*
        public void demoCard(){

            // Create a Card
            EventCard card = new EventCard(getActivity(),"Vaishali FC Road","rahul,Vipluv,SId,Ani","24Jan","9:00");

            // Create a CardHeader
            CardHeader header = new CardHeader(getActivity());
            header.setTitle("Bday Party");

            // Add Header to card
            card.addCardHeader(header);


            CardThumbnail thumb = new CardThumbnail(getActivity());
            thumb.setDrawableResource(R.drawable.ic_launcher);
            card.addCardThumbnail(thumb);

            Log.i("freakout",card.friends+card.time);
            // Set card in the cardView
            CardView cardView = (CardView) getActivity().findViewById(R.id.carddemo);
            cardView.setCard(card);

        }
    */
    public void eventCardList() {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */
        SharedPreferences eventsCheck=getActivity().getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        eventList=eventsCheck.getString("eventList","");
        if(!(eventList.equals(""))) {
            try {
                JSONArray eventsArray = new JSONArray(eventList);
                ArrayList<Card> cards = new ArrayList<Card>();

                for (int i = 0; i < eventsArray.length(); i++) {
                    // Create a Card
                    JSONObject event=eventsArray.getJSONObject(i);
                    EventCard card = new EventCard(getActivity(), event.getString("id"),event.getString("name"),event.getString("venue"),event.getString("venueLat"),event.getString("venueLon"),event.getString("venueAddress"), event.getString("date"), event.getString("time"),event.getString("status"),event.getString("creator"));

                    // Create a CardHeader
                    CardHeader header = new CardHeader(getActivity());
                    //header.setTitle("Bday Party");

                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    //card.setBackgroundResourceId(R.drawable.card_selector);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);
                //mCardArrayAdapter.setInnerViewTypeCount(1);


                CardListView listView = (CardListView) getActivity().findViewById(R.id.myList);

                AnimationAdapter animCardArrayAdapter = new ScaleInAnimationAdapter(mCardArrayAdapter);
                animCardArrayAdapter.setAbsListView(listView);


                if (listView != null) {
//            listView.setAdapter(mCardArrayAdapter);


                    listView.setExternalAdapter(animCardArrayAdapter,mCardArrayAdapter);


                }
            } catch(Exception e) {e.printStackTrace();}
        }

    }


    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    */





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */



    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    */


}
