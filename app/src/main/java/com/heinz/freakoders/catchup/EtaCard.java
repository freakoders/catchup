package com.heinz.freakoders.catchup;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by rahul on 28/1/15.
 */
public class EtaCard extends Card {
    /**
     * Constructor with a custom inner layout
     * @param context
     *
     */

    protected String name,number,eta,distance;


    public EtaCard(Context context) {
        this(context, R.layout.eta_card);
    }

    public EtaCard(Context context, String name, String number, String eta, String distance) {

        this(context, R.layout.eta_card);
        this.name=name;
        this.number=number;
        this.eta=eta;
        this.distance=distance;


        Log.d("myTag", "ETA Card constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public EtaCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

        //Set a OnClickListener listener
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                Toast.makeText(getContext(), "eta="+eta+" distance="+distance, Toast.LENGTH_SHORT).show();
               //Call refresh
                card.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");

        TextView ename = (TextView) view.findViewById(R.id.friend_name);
        TextView enumber = (TextView) view.findViewById(R.id.friend_number);
        TextView eeta = (TextView) view.findViewById(R.id.friend_eta);
        TextView edistance = (TextView) view.findViewById(R.id.friend_distance);

        ename.setText(name);
        enumber.setText(number);
        eeta.setText(eta+" min.");
        edistance.setText(distance+" mtr.");





    }
}

