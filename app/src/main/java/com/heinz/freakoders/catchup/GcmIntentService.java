package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationClient;
import com.heinz.freakoders.catchup.GcmBroadcastReceiver;
import com.heinz.freakoders.catchup.GcmSender;
import com.heinz.freakoders.catchup.MainActivity;
import com.heinz.freakoders.catchup.PlayServiceTester;
import com.heinz.freakoders.catchup.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

//server   http://176.58.115.9/

public class GcmIntentService extends IntentService
        implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {
    public static final int NOTIFICATION_ID = 1;
    AtomicInteger msgId = new AtomicInteger();
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    public static LocationClient servLocClient;
    public static double servLAT=600;
    public static double servLON=600;
    String servPhNum;
    public  static Context servContext;
    public final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static String servSENDER_ID = "233688222995";
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        Log.i("freakout","in the intentService");
        String messageType = gcm.getMessageType(intent);
        servPhNum=getPhNum(GcmIntentService.this);
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(),false,"none");
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString(),false,"none");
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                //MainActivity.locClient.connect();
                Log.i("freakout","valid msg detected");
                //Log.i("freakout","contents "+extras.get("messageType").toString());
                /*
                Set<String>keys=extras.keySet();
                for(String key:keys) {
                    Log.i("freakout",key+extras.get(key).toString());
                }
                */


                int msgType=Integer.parseInt(extras.get("messageType").toString());
                switch(msgType) {
                    case 1:
                        Log.i("freakout","location request detected.");
                        sendLocationUpdate();
                        break;
                    case 2:
                        Log.i("freakout","FriendMsg detected");
                        showNearbyFriends(extras);
                        break;
                    case 3:
                        Log.i("freakout","Events list received");
                        showEventList(extras);
                        break;
                    case 4:
                        Log.i("freakout","Contact list received");
                        updateContactList(extras);
                        break;
                    case 101:
                        Log.i("freakout","Event Notification received");
                        showEventNotification(extras);
                        break;
                    case 6:
                        Log.i("freakout","MemberList received");
                        updateMemberList(extras);
                        break;


                    case 7:
                        getPlaceSuggestion(extras);
                        break;


                    case 8:
                        Log.i("freakout","SuggestionList received");
                        showEventSuggestionList(extras);
                        break;

                    case 9:
                        Log.i("freakout","EtaList received");
                        showEtaList(extras);
                        break;

                    //10x: NOTIFICATIONS

                    case 102:
                        showNewSuggestion(extras);
                        break;

                    //20x: REMINDERS

                    case 201:

                        eventReminder(extras);
                        break;

                    //30x: ACKS

                    case 301:
                        //Registration ACK
                        Intent registeredAckIntent=new Intent("registeredAck");
                        registeredAckIntent.putExtra("type",0);
                        sendBroadcast(registeredAckIntent);
                        break;

                    case 302:
                        //Registration ACK
                        Intent contactsAckIntent=new Intent("registeredAck");
                        contactsAckIntent.putExtra("type",1);
                        sendBroadcast(contactsAckIntent);
                        break;



                    default:
                        Log.i("freakout","msgtype unknown: " + extras.toString());
                        break;
                }


                /*
                for (int i=0; i<5; i++) {
                    Log.i("mytag", "Working... " + (i + 1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.i("mytag", "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification("Received: " + extras.toString());
                Log.i("mytag", "Received: " + extras.toString());
                */
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendLocationUpdate() {
        Log.i("freakout","sending location update");
        try {
            servContext=getApplicationContext();
            PlayServiceTester playTest=new PlayServiceTester();
            if(playTest.checkPlayServices(this)==true) {
                // MainActivity.locClient=new LocationClient(MainActivity.mainContext,);
                servLocClient = new LocationClient(servContext, GcmIntentService.this, GcmIntentService.this);
                servLocClient.connect(); //newline
                int ctr=0;
                while(servLAT>500 || servLON>500 && ctr<20) {
                    sleep(100);
                    ctr++;
                }

                Log.i("freakout", "between");
                //Location currentLocation = MainActivity.locClient.getLastLocation();
                //MainActivity.LAT = currentLocation.getLatitude();
                //MainActivity.LON = currentLocation.getLongitude();
                //Toast.makeText(getApplicationContext(),"LAT: "+LAT+"LON: "+LON+"",Toast.LENGTH_LONG).show();
                servLocClient.disconnect(); //newline
                //sendNotification(""+servLAT+","+servLON);
                Log.i("freakout","PlayServices");
            }
            else {
                Log.i("freakout","no PlayServices");
            }
        }
        catch (Exception e){
            Log.i("freakout","catch PlayServices");
            //Toast.makeText(getApplicationContext(),"Failed to get LAT/LON",Toast.LENGTH_LONG).show();
        }
        Log.i("freakout","after PlayServices");
        //sendNotification(""+servLAT+","+servLON);
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg="";
                Bundle data = new Bundle();
                data.putString("url","/user/updatelocation");
                data.putString("phoneNumber",servPhNum);
                data.putString("latitude", ""+servLAT);
                data.putString("longitude",""+servLON);
                String id = Integer.toString(msgId.incrementAndGet());
                new GcmSender(data,GcmIntentService.this).execute(null,null,null);
                msg="Sent message";

                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i("freakout",msg);
            }
        }.execute(null,null,null);

    }

    private void showNearbyFriends(Bundle data) {
        String nearbyPhoneNumbers= data.get("nearbyFriendList").toString();
//        if(nearbyPhoneNumbers.equals("[]")){
//            Log.i("freakout","Empty friend list detected");
//            int errorCode= Integer.parseInt(data.get("errorCode").toString());
//            switch(errorCode){
//                case 0:
//                    Toast.makeText(GcmIntentService.this,"NO nearby friends.",Toast.LENGTH_SHORT).show();
//                    break;
//                case 1:
//                    Toast.makeText(GcmIntentService.this,"Invalid phone number or user not registered",Toast.LENGTH_SHORT).show();
//                    break;
//                case 2:
//                    Toast.makeText(GcmIntentService.this,"current Location not known",Toast.LENGTH_SHORT).show();
//                    break;
//                case 3:
//                    Toast.makeText(GcmIntentService.this,"NO friend List!",Toast.LENGTH_SHORT).show();
//                    break;
//            }
//        }
//        else{
            Log.i("freakout","Friend list detected!");
            String contactNames="";
            Log.i("freakout",nearbyPhoneNumbers);
            Intent textViewUpdater=new Intent("nearbyFriendsMsg");
            //List<String> phoneNumberList= Arrays.asList(nearbyPhoneNumbers.split(","));
            try {
                JSONArray phoneNumberList = new JSONArray(nearbyPhoneNumbers);
                for (int i=0;i<phoneNumberList.length();i++) {
                    JSONObject phoneNumber=phoneNumberList.getJSONObject(i);
                    String name=getContactName(phoneNumber.getString("phoneNumber"));
                    if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
                    else phoneNumber.put("name",name);
                    phoneNumberList.put(i,phoneNumber);
                    //contactNames = contactNames + getContactName(phoneNumber) + ",";
                }
                //Intent textViewUpdater = new Intent("nearbyFriendsMsg");
                textViewUpdater.putExtra("nearbyFriendList", phoneNumberList.toString());
            } catch(Exception e) {
                e.printStackTrace();
            }
            textViewUpdater.putExtra("errorCode",data.getString("errorCode"));
            sendBroadcast(textViewUpdater);


 //       }
        sendNotification("Friends found nearby!",false,"NearbyFriends");
   }

    private void showEventList(Bundle data) {
       /*
       HERE I NEED TO SEND A BROADCAST TO THE NEW RECEIVER
        */
        Log.i("freakout","Event List detected.");
        storeEventList(GcmIntentService.this,data.getString("eventList"));
        Intent textViewUpdater=new Intent("eventsMsg");

        //textViewUpdater.putExtra("eventList",data.getString("eventList"));
        sendBroadcast(textViewUpdater);


       // Intent eventListIntent=new Intent(GcmIntentService.this,EventsActivity.class);
       // eventListIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       // eventListIntent.putExtras(data);
        //startActivity(eventListIntent);
        /*
        String AllEvents= data.get("eventsList").toString();
        ExperimentFragment.textView2.setText(AllEvents);
        */

    }

    private void updateContactList(Bundle data) {
        String phoneNumbers= data.get("friendList").toString();
        try {
            JSONArray phoneNumberList = new JSONArray(phoneNumbers);
            for (int i=0;i<phoneNumberList.length();i++) {
                JSONObject phoneNumber=phoneNumberList.getJSONObject(i);
                String name=getContactName(phoneNumber.getString("phoneNumber"));
                if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
                else phoneNumber.put("name",name);
                phoneNumberList.put(i,phoneNumber);
                //contactNames = contactNames + getContactName(phoneNumber) + ",";
            }
            storeContactList(GcmIntentService.this,phoneNumberList.toString());


            //textViewUpdater.putExtra("nearbyFriendList", phoneNumberList.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }
        Intent textViewUpdater = new Intent("refreshContactsMsg");
        //textViewUpdater.putExtra("errorCode",data.getString("errorCode"));
        sendBroadcast(textViewUpdater);
    }

    private  void getPlaceSuggestion(Bundle data)
    {

        Intent suggestionUpdater=new Intent("suggestionsMsg");
       //textViewUpdater.putExtra("eventList",data.getString("eventList"));
        suggestionUpdater.putExtras(data);
        sendBroadcast(suggestionUpdater);


    }

    private void eventReminder(Bundle data)
    {
        String notification=data.get("notification").toString();
        String time=data.get("eventTime").toString();
        String name=data.get("eventName").toString();
        sendNotification("Event:\""+name+"\" starts at "+time,true,"none");



    }


    private void showEventNotification(Bundle data) {
        sendNotification("You have been added to a new event!",true,"Experiment");
        Bundle sendData = new Bundle();
        sendData.putString("url", "/user/getevents");
        sendData.putString("phoneNumber", getPhNum(GcmIntentService.this));
        new GcmSender(sendData, GcmIntentService.this).execute(null, null, null);
    }

    private void showNewSuggestion(Bundle data) {
        Intent showSuggestionIntent=new Intent("newSuggestionMsg");
        try {
            JSONObject phoneNumber = new JSONObject(data.getString("suggestion"));
            String name=getContactName(phoneNumber.getString("phoneNumber"));
            if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
            else phoneNumber.put("name",name);
            showSuggestionIntent.putExtra("suggestion", phoneNumber.toString());
            showSuggestionIntent.putExtra("id", data.getString("id"));
        } catch(Exception e) {e.printStackTrace();}
        sendBroadcast(showSuggestionIntent);
    }


    private void updateMemberList(Bundle data) {
        JSONArray phoneNumberList=null;
        try {
            String phoneNumbers= data.get("memberList").toString();
            phoneNumberList = new JSONArray(phoneNumbers);
            for (int i=0;i<phoneNumberList.length();i++) {
                JSONObject phoneNumber=phoneNumberList.getJSONObject(i);
                String name=getContactName(phoneNumber.getString("phoneNumber"));
                if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
                else phoneNumber.put("name",name);
                phoneNumberList.put(i,phoneNumber);
                //contactNames = contactNames + getContactName(phoneNumber) + ",";
            }



            //textViewUpdater.putExtra("nearbyFriendList", phoneNumberList.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }
        Intent memberListIntent=new Intent("memberListMsg");
        memberListIntent.putExtra("memberList",phoneNumberList.toString());
        memberListIntent.putExtra("id",data.getString("id"));
        sendBroadcast(memberListIntent);
    }

    private void showEtaList(Bundle data) {
        JSONArray phoneNumberList=null;
        String phoneNumbers= data.get("etaList").toString();
        try {
            phoneNumberList = new JSONArray(phoneNumbers);
            for (int i=0;i<phoneNumberList.length();i++) {
                JSONObject phoneNumber=phoneNumberList.getJSONObject(i);
                String name=getContactName(phoneNumber.getString("phoneNumber"));
                if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
                else phoneNumber.put("name",name);
                phoneNumberList.put(i,phoneNumber);
                //contactNames = contactNames + getContactName(phoneNumber) + ",";
            }
            //storeContactList(GcmIntentService.this,phoneNumberList.toString());


            //textViewUpdater.putExtra("nearbyFriendList", phoneNumberList.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }
        Intent textViewUpdater = new Intent("etaMsg");
        textViewUpdater.putExtra("etaList",phoneNumberList.toString());
        textViewUpdater.putExtra("id",data.getString("id"));
        //textViewUpdater.putExtra("errorCode",data.getString("errorCode"));
        sendBroadcast(textViewUpdater);
    }





    private String getPhNum(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum=prefs.getString(MainActivity.PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");

            return "";
        }
        Log.i("freakout","your phone number "+phNum);
        return phNum;
    }

    private void showEventSuggestionList(Bundle data) {
        Intent memberListIntent=new Intent("eventSuggestionListMsg");
        JSONArray phoneNumberList=null;
        try {
            String phoneNumbers= data.get("suggestionList").toString();
            phoneNumberList = new JSONArray(phoneNumbers);
            for (int i=0;i<phoneNumberList.length();i++) {
                JSONObject phoneNumber=phoneNumberList.getJSONObject(i);
                String name=getContactName(phoneNumber.getString("phoneNumber"));
                if(name.equals("")) phoneNumber.put("name",phoneNumber.getString("displayName"));
                else phoneNumber.put("name",name);
                phoneNumberList.put(i,phoneNumber);
                //contactNames = contactNames + getContactName(phoneNumber) + ",";
            }



            //textViewUpdater.putExtra("nearbyFriendList", phoneNumberList.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }

        memberListIntent.putExtra("suggestionList",phoneNumberList.toString());
        memberListIntent.putExtra("id",data.getString("id"));
        sendBroadcast(memberListIntent);

    }





    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg ,boolean alarm,String activity) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;


            contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, MainActivity.class), 0);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_category_food)
                        .setContentTitle("CatchUp!")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);
        if(alarm==true) {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);
        }
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }


    private void storeEventList(Context context, String eventList) {
        final SharedPreferences prefs = getGCMPreferences(context);
        Log.i("freakout", "Saving new events ");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("eventList", eventList);
        //editor.putString(MainActivity.PHONE_PREFIX,phonePrefix);
        editor.commit();
    }

    private String getContactName(String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));
        Cursor cs= GcmIntentService.this.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},null,null,null);
        cs.moveToFirst();
        if(cs.getCount()>0)
            return cs.getString(cs.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        cs.close();
        return "";

    }


    private void storeContactList(Context context, String contactList) {
        final SharedPreferences prefs = getGCMPreferences(context);
        Log.i("freakout", "Saving new contacts:  "+contactList);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("contactList", contactList);
        //editor.putString(MainActivity.PHONE_PREFIX,phonePrefix);
        editor.commit();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.
        //Toast.makeText(getApplicationContext(),"locClient connected!",Toast.LENGTH_LONG).show();
        try {
            Log.i("freakout","Connected");
            Location currentLocation = servLocClient.getLastLocation();
            servLAT = currentLocation.getLatitude();
            servLON = currentLocation.getLongitude();
            //Toast.makeText(getApplicationContext(),"LAT: "+LAT+"LON: "+LON+"",Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            Log.i("freakout","caught connected");
            //Toast.makeText(getApplicationContext(),"Failed to get LAT/LON",Toast.LENGTH_LONG).show();
        }
        //locClient.disconnect();


    }



    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the next section.
    }
}