package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import at.markushi.ui.CircleButton;


public class RegistrationActivity extends Activity {

    CircleButton nameEntryButton;
    EditText nameField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        nameField=new EditText(RegistrationActivity.this);
        nameField=(EditText)findViewById(R.id.nameField);
        nameEntryButton=new CircleButton(RegistrationActivity.this);
        nameEntryButton=(CircleButton) findViewById(R.id.nameEntryButton);
        nameEntryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String name=new String();
                name="";
                name=nameField.getText().toString();
                Log.i("freakout","name is "+name);

                if (name.matches("")==false) {
                    Log.i("freakout","Name isn't blank");
                    storeUserName(RegistrationActivity.this, name);
                    //Toast.makeText(RegistrationActivity.this,"We're absolutely through.",Toast.LENGTH_SHORT).show();
                    Intent registeredIntent = new Intent(RegistrationActivity.this, MainActivity.class);
                    registeredIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(registeredIntent);
                    RegistrationActivity.this.finish();
                }
                else {
                    Toast.makeText(RegistrationActivity.this,"Please enter a name.",Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registration, menu);







        return true;
    }


    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void storeUserName(Context context, String userName) {
        final SharedPreferences prefs = getGCMPreferences(context);
        Log.i("freakout", "Saving User Name ");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MainActivity.USER_NAME, userName);
        editor.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
