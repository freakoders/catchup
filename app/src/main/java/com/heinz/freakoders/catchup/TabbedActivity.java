package com.heinz.freakoders.catchup;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.ArrayList;
import java.util.List;


public class TabbedActivity extends FragmentActivity {
    NearbyFriendsBroadcastReceiver mBroadCastReceiver;
    EventsBroadcastReceiver eventsBroadcastReceiver;
    MyPageAdapter pageAdapter;
    public static final String PHONE_NUM="phoneNumber";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBroadCastReceiver = new NearbyFriendsBroadcastReceiver();
        eventsBroadcastReceiver= new EventsBroadcastReceiver();
        TabbedActivity.this.registerReceiver(mBroadCastReceiver, new IntentFilter("nearbyFriendsMsg"));
        TabbedActivity.this.registerReceiver(eventsBroadcastReceiver,new IntentFilter("eventsMsg"));
        setContentView(R.layout.activity_tabbed);
        List<Fragment> fragments = getFragments();


        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        ViewPager pager =(ViewPager)findViewById(R.id.viewpager);
        pager.setAdapter(pageAdapter);


        PlayServiceTester playTest=new PlayServiceTester();
       // if (playTest.checkPlayServices(TabbedActivity.this)==true) {
            //Bundle data = new Bundle();
            //Log.i("freakout","yoyoyo");

           // data.putString("heyTab", "earthTab");
            //new GcmSender(data, TabbedActivity.this).execute(null, null, null);
       // }

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setBackgroundColor(getResources().getColor(R.color.catchup_dark));
        tabs.setTabBackground(R.drawable.tab_selector);
        tabs.setViewPager(pager);

        Bundle data = new Bundle();
        data.putString("url", "/user/getevents");
        data.putString("phoneNumber", getPhNum(TabbedActivity.this));
        new GcmSender(data, TabbedActivity.this).execute(null, null, null);





    }

    private String getPhNum(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum=prefs.getString(PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");
            return "";
        }
        //TabbedActivity.this.phonePrefix=prefs.getString(PHONE_PREFIX,"");
        return phNum;
    }





    /*
    HERE I NEED TO MAKE A NEW RECEIVER FOR EVENTS AND SEND RECEIVED DATA TO THE FRAGMENT
     */

    private class EventsBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout","events Broadcast received");
            //storeEventList(context,intent.getStringExtra("eventList"));
            ExperimentFragment topBarFragment=(ExperimentFragment)getSupportFragmentManager().findFragmentByTag("experimentFragment");
            topBarFragment.eventCardList();
            //ExperimentFragment.textView2.setText(intent.getExtras().get("eventList").toString());
        }
    }
    private class NearbyFriendsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("freakout", "nearbyfriend broadcast received");
            //NearbyFriendsFragment.friendDisplayView.setText(intent.getExtras().get("nearbyFriendList").toString());
            NearbyFriendsFragment topBarFragment = (NearbyFriendsFragment)getSupportFragmentManager().findFragmentByTag("nearbyFriendsFragment");
            Log.i("freakout",""+topBarFragment);
            if(intent.hasExtra("nearbyFriendList"))
                topBarFragment.nearbyFriendCardList(intent.getExtras());
            else Log.i("freakout","friendless");
            //NearbyFriendsFragment.updateNearbyFriends(intent.getExtras());

        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void storeEventList(Context context, String eventList) {
        final SharedPreferences prefs = getGCMPreferences(context);
        Log.i("freakout", "Saving new events ");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("eventList", eventList);
        //editor.putString(MainActivity.PHONE_PREFIX,phonePrefix);
        editor.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBroadCastReceiver = new NearbyFriendsBroadcastReceiver();
        eventsBroadcastReceiver=new EventsBroadcastReceiver();
        TabbedActivity.this.registerReceiver(mBroadCastReceiver, new IntentFilter("nearbyFriendsMsg"));
        TabbedActivity.this.registerReceiver(eventsBroadcastReceiver, new IntentFilter("EventsMsg"));

        Bundle data = new Bundle();
        data.putString("url", "/user/getevents");
        data.putString("phoneNumber", getPhNum(TabbedActivity.this));
        new GcmSender(data, TabbedActivity.this).execute(null, null, null);
    }



    @Override
    public void onStop() {

        super.onStop();
        try {
            if (mBroadCastReceiver != null)
                TabbedActivity.this.unregisterReceiver(mBroadCastReceiver);
            if (eventsBroadcastReceiver != null)
                TabbedActivity.this.unregisterReceiver(eventsBroadcastReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();

        try {
            if (mBroadCastReceiver != null)
                TabbedActivity.this.unregisterReceiver(mBroadCastReceiver);
            if (eventsBroadcastReceiver != null)
                TabbedActivity.this.unregisterReceiver(eventsBroadcastReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
        if(mBroadCastReceiver!=null)
            TabbedActivity.this.unregisterReceiver(mBroadCastReceiver);
        if(eventsBroadcastReceiver!=null)
            TabbedActivity.this.unregisterReceiver(eventsBroadcastReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();

        fList.add(ExperimentFragment.newInstance("JustaTry"));
        //fList.add(Testing.newInstance());
        NearbyFriendsFragment temp=NearbyFriendsFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .add(new NearbyFriendsFragment(),"nearbyFriendsFragment")
                .commit();
        getSupportFragmentManager()
                .beginTransaction()
                .add(new ExperimentFragment(),"experimentFragment")
                .commit();
        fList.add(temp);
//        fList.add(NearbyFriendsFragment.newInstance());
//        getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.fragment_holder, NearbyFriendsFragment)
//                .commit();



        return fList;

    }



}
