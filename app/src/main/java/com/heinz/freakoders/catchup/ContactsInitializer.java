package com.heinz.freakoders.catchup;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sid on 9/9/14.
 */
public class ContactsInitializer {
    String[] contactList;
    public int contactListIndex;
    public ContactsInitializer() {
        contactList=new String[200];
        contactListIndex=0;
    }
    protected int getContactList(ContentResolver cr,String defaultCountryCode) {
        int totalContactCount=0,contactCount=0;

        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        if(contactCount==100) {
                            contactListIndex++;
                            totalContactCount+=contactCount;
                            contactCount=0;
                        }
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //Toast.makeText(InitFriendsActivity.this, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                        String validPhoneNo=getValidNumber(phoneNo,defaultCountryCode);
                        contactList[contactListIndex]=contactList[contactListIndex]+","+validPhoneNo;
                        contactCount++;
                    }
                    pCur.close();
                }
            }
        }
        cur.close();
        //Log.i("freakout",contactList);
        Log.i("freakout","contact count="+totalContactCount);
        return contactListIndex+1;
    }

    protected String getValidNumber(String phoneNo,String defaultCountryCode) {
        //return phoneNo.length() <= 10 ? phoneNo : phoneNo.substring(phoneNo.length() - 10);
        Pattern DIALING_PLAN = Pattern.compile("^\\+|(00)|(0)");
        String internationalPrefix ="00";
        String defaultCountryNationalPrefix ="0";
        String builtPhoneNo="";
        for(int i=0;i<phoneNo.length();i++){
            if((phoneNo.charAt(i)<=57 && phoneNo.charAt(i)>=48)||phoneNo.charAt(i)==43 ){
                builtPhoneNo+=phoneNo.charAt(i);
            }
        }

        if (builtPhoneNo.startsWith ("+")) // already in desired format
            return builtPhoneNo;
        if (builtPhoneNo.startsWith(internationalPrefix))
            return builtPhoneNo.replaceFirst(internationalPrefix, "+");
        else if (builtPhoneNo.startsWith(defaultCountryNationalPrefix))
            return builtPhoneNo.replaceFirst(defaultCountryNationalPrefix, defaultCountryCode);
        else
            return defaultCountryCode+builtPhoneNo;

    }

    public void initializeContacts(GoogleCloudMessaging gcm,String senderId,Context context,String phNum) {
        //String senderId="274953604138";
        Log.i("freakout","Initializing contacts");
        for(int i=0;i<=contactListIndex;i++) {
            Bundle data = new Bundle();
            data.putString("url", "/user/init/friendlist");
            data.putString("contactList", contactList[i]);
            data.putString("phoneNumber", phNum);
            try {
                gcm.send(senderId + "@gcm.googleapis.com", "1", data);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //new GcmSender(data,context).execute(null,null,null);
        }
    }

    public void initializeContacts(Context context,String phNum) {
        //String senderId="274953604138";
        Log.i("freakout","Initializing contacts");
        for(int i=0;i<=contactListIndex;i++) {
            Bundle data = new Bundle();
            data.putString("url", "/user/init/friendlist");
            data.putString("contactList", contactList[i]);
            data.putString("phoneNumber", phNum);

            new GcmSender(data,context).execute(null,null,null);
        }
    }
}
