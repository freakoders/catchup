package com.heinz.freakoders.catchup;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by vipluv on 2/2/15.
 */
public class EventMemberCard extends Card {

    /**
     * Constructor with a custom inner layout
     * @param context
     *
     */

    protected String name,number;
    protected int status;

    public EventMemberCard(Context context) {
        this(context, R.layout.event_member_card);
    }

    public EventMemberCard(Context context,String name,String number,int status) {

        this(context, R.layout.event_member_card);
        this.name=name;
        this.number=number;
        this.status=status;

        Log.d("myTag", "Friend Card constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public EventMemberCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

//        //Set a OnClickListener listener
//        setOnClickListener(new Card.OnCardClickListener() {
//            @Override
//            public void onClick(Card card, View view) {
//                //Toast.makeText(getContext(), "Member card=" + name, Toast.LENGTH_SHORT).show();
//
//
//            }
//        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");

        TextView vname = (TextView) view.findViewById(R.id.member_name);
        TextView vnumber = (TextView) view.findViewById(R.id.member_number);

        vname.setText(name);

        vnumber.setText(number);


        if (status==1) {

            //card.setBackgroundResourceId(R.drawable.demo_card_selector_color2);
            //card.setBackgroundColorResourceId(R.color.catchup_accent);
            ImageView img= (ImageView) view.findViewById(R.id.member_status);
            img.setImageResource(R.drawable.accepted_event_green);
            //TextView ttt=(TextView)view.findViewById(R.id.friend_number);
            //ttt.setText("xyz"+name);
//            LinearLayout back = (LinearLayout) view.findViewById(R.id.member_card_back);
  //          back.setBackgroundColor(getContext().getResources().getColor(R.color.accept_event));


        } else if(status==-1) {
            ImageView img= (ImageView) view.findViewById(R.id.member_status);
            img.setImageResource(R.drawable.rejected_event_red);

           // LinearLayout back = (LinearLayout) view.findViewById(R.id.member_card_back);
          //  back.setBackgroundColor(getContext().getResources().getColor(R.color.reject_event));



        }

    }


}
