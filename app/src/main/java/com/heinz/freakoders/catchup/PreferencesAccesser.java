package com.heinz.freakoders.catchup;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by rahul on 31/1/15.
 */
public class PreferencesAccesser {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String USER_NAME = "displayName";
    public static final String PHONE_NUM="phoneNumber";
    public static final String PHONE_PREFIX="countryCode";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    SharedPreferences prefs;


    public PreferencesAccesser(Context context) {
        prefs = getGCMPreferences(context);
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    public void storeRegistrationId(String regId,int appVersion) {
        //final SharedPreferences prefs = getGCMPreferences(context);
        //int appVersion = getAppVersion(context);
        Log.i("freakout", "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }



    public String getPhNum() {
        String phNum=prefs.getString(PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");
            return "";
        }
        //TabbedActivity.this.phonePrefix=prefs.getString(PHONE_PREFIX,"");
        return phNum;
    }
}
