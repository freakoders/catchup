package com.heinz.freakoders.catchup;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by sid on 9/10/14.
 */
public class GcmSender extends AsyncTask<Void,Void,Void> {
    public static final String SENDER_ID = "233688222995";
    static AtomicInteger msgId = new AtomicInteger();
    private Bundle data;
    private Context context;
    public GcmSender(Bundle inBundle,Context inContext) {
        data=inBundle;
        context=inContext;

    }

    @Override
    protected Void doInBackground(Void... params) {

        Log.i("freakout","inside gcmsender doInBack");
        try {
            PlayServiceTester playTest=new PlayServiceTester();
            if (playTest.checkPlayServices(context)==true) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
                Log.i("freakout", gcm.toString());
                String id = Integer.toString(msgId.incrementAndGet());
                gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
                Log.i("freakout", "Sent message");
            }
        } catch (Exception ex) {
            Log.i("freakout","UnSent message");
            ex.printStackTrace();
            //Toast.makeText(TabbedActivity.mainContext,"Error :" + ex.getMessage(),Toast.LENGTH_LONG).show();
        }


        return null;
    }

    protected void onPostExecute(Void result) {
        synchronized (InitializationActivity.monitor) {
            InitializationActivity.gcmSent=true;
            InitializationActivity.monitor.notifyAll();
        }
        Log.i("freakout","sent message onPostExecute");

    }
}
