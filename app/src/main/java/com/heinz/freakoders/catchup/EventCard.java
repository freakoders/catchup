package com.heinz.freakoders.catchup;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by sid on 1/12/15.
 */
public class EventCard extends Card {
    private String[] monthLetters={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    protected String eventId,venue,venueLat,venueLon,venueAddress,date,time,name,status,creator;


    /**
     * Constructor with a custom inner layout
     * @param context
     */
    public EventCard(Context context) {
        this(context, R.layout.row_card);
    }

    public EventCard(Context context,String id,String name,String venue,String venueLat,String venueLon,String venueAddress,String date,String time,String status,String creator) {

        this(context, R.layout.row_card);
        this.eventId =id;
        this.name=name;
        this.venue=venue;
        this.venueLat=venueLat;
        this.venueLon=venueLon;
        this.venueAddress=venueAddress;
        //this.friends=friends+"...";
        this.date=date;
        this.time=time;
        this.status=status;
        this.creator=creator;
        Log.d("myTag","EventCard constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public EventCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

        //Set a OnClickListener listener
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                //Toast.makeText(getContext(), "Click Listener card="+name, Toast.LENGTH_SHORT).show();
                Intent eventDetailsIntent;
                Log.i("freakout","status="+status);
                if(creator.equals(new PreferencesAccesser(getContext()).getPhNum())) eventDetailsIntent=new Intent(getContext(),AcceptedEventDetailsActivity.class);
                else {
                    if (status.equals("1") || status.equals("10"))
                        eventDetailsIntent = new Intent(getContext(), AcceptedEventDetailsActivity.class);
                    else
                        eventDetailsIntent = new Intent(getContext(), PendingEventDetailsActivity.class);
                }
                eventDetailsIntent.putExtra("id", eventId);
                eventDetailsIntent.putExtra("name", name);
                eventDetailsIntent.putExtra("venue", venue);
                eventDetailsIntent.putExtra("venueLat", venueLat);
                eventDetailsIntent.putExtra("venueLon", venueLon);
                eventDetailsIntent.putExtra("venueAddress", venueAddress);
                eventDetailsIntent.putExtra("date", date);
                eventDetailsIntent.putExtra("time", time);
                eventDetailsIntent.putExtra("status", status);
                eventDetailsIntent.putExtra("creator", creator);

                getContext().startActivity(eventDetailsIntent);


            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");
        TextView vvenue = (TextView) view.findViewById(R.id.venue);
        //TextView vfriends = (TextView) view.findViewById(R.id.friends);
        //TextView vdate = (TextView) view.findViewById(R.id.date);
        //TextView vtime = (TextView) view.findViewById(R.id.time);
        TextView vname=(TextView) view.findViewById(R.id.event_name);
        Log.i("freakout",""+getContext().getResources().getColor(R.color.catchup_accent));
        String monthName=this.date.split("-")[1];
        String year="'"+this.date.split("-")[2].substring(2);
        TextView month_text=(TextView)  view.findViewById(R.id.monthText);

        month_text.setText(monthName+" "+year);
        month_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.monthTextSize));

        TextView day_text=(TextView)  view.findViewById(R.id.dayText);

        day_text.setText(this.date.split("-")[0]);
        day_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.dayTextSize));

        TextView time_text=(TextView)  view.findViewById(R.id.timeText);
        time_text.setText(this.time);
        time_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.timeTextSize));
        vvenue.setText(this.venue);
        vname.setText(this.name);
        //vfriends.setText(friends);
        //vdate.setText(date);
        //vtime.setText(time);

    }
}
