package com.heinz.freakoders.catchup;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by vipluv on 3/2/15.
 */
public class SuggestionCard extends Card {
    private String[] monthLetters={"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
    protected String venue,date,time,byName,byNumber;


    /**
     * Constructor with a custom inner layout
     * @param context
     */
    public SuggestionCard(Context context) {
        this(context, R.layout.suggestion_card);
    }

    public SuggestionCard(Context context,String byName,String byNumber,String venue,String date,String time) {

        this(context, R.layout.suggestion_card);

        this.byName=byName;
        this.byNumber=byNumber;
        this.venue=venue;
        //this.friends=friends+"...";
        this.date=date;
        this.time=time;
        Log.d("myTag", "Suggestion Card constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public SuggestionCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

        //Set a OnClickListener listener
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {


            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");
        TextView vvenue = (TextView) view.findViewById(R.id.suggest_venue);

        TextView vsuggestName = (TextView) view.findViewById(R.id.suggest_by_name);
        TextView vsuggestNumber = (TextView) view.findViewById(R.id.suggest_by_number);

        vvenue.setText(this.venue);
        vsuggestName.setText(this.byName);
        vsuggestNumber.setText(this.byNumber);




        //TextView vfriends = (TextView) view.findViewById(R.id.friends);
        //TextView vdate = (TextView) view.findViewById(R.id.date);
        //TextView vtime = (TextView) view.findViewById(R.id.time);

        Log.i("freakout",""+getContext().getResources().getColor(R.color.catchup_accent));
        String monthName=this.date.split("-")[1];
        String year="'"+this.date.split("-")[2].substring(2);
        TextView month_text=(TextView)  view.findViewById(R.id.monthText);

        month_text.setText(monthName+" "+year);
        month_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.monthTextSize));

        TextView day_text=(TextView)  view.findViewById(R.id.dayText);

        day_text.setText(this.date.split("-")[0]);
        day_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.dayTextSize));

        TextView time_text=(TextView)  view.findViewById(R.id.timeText);
        time_text.setText(this.time);
        time_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimension(R.dimen.timeTextSize));


        //vfriends.setText(friends);
        //vdate.setText(date);
        //vtime.setText(time);

    }
}
