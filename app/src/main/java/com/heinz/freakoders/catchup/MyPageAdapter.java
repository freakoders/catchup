package com.heinz.freakoders.catchup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;

import java.util.List;

/**
 * Created by sid on 12/22/14.
 */

/*
class MyPageAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;

    private final String[] TITLES = { "Events", "Friends Nearby"};



    public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    ///////////


    /*
    @Override
    public Fragment getItem(int position) {
        return SuperAwesomeCardFragment.newInstance(position);
    }
    */
/*
}
*/

class MyPageAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    final int PAGE_COUNT = 3;
    private final int[] ICONS = { R.drawable.event_icon_selector, R.drawable.nearbyfriends_icon_selector,R.drawable.ic_me};


    private List<Fragment> fragments;

    private final String[] TITLES = { "Events", "Friends Nearby","ME"};



    public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    ///////////


    /*
    @Override
    public Fragment getItem(int position) {
        return SuperAwesomeCardFragment.newInstance(position);
    }
    */
    @Override
    public int getPageIconResId(int position) {
        // TODO Auto-generated method stub
        return ICONS[position];
    }

}