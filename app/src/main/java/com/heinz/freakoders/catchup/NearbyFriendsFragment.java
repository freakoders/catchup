package com.heinz.freakoders.catchup;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.internal.in;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

/**
 * Created by sid on 1/10/15.
 */
public class NearbyFriendsFragment extends Fragment {



    public static final String PHONE_PREFIX="countryCode";
    public static final String PHONE_NUM="phoneNumber";
    public static ContentResolver cr;
    GoogleCloudMessaging gcm;
    public String phonePrefix;
    public String phoneNumber;
    RefreshBroadcastReceiver mBroadCastReceiver;
    public static TextView friendDisplayView;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String eventList;
    //private String mParam2;

    //private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment ExperimentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NearbyFriendsFragment newInstance() {
        Log.i("freakout", "newInstance");
        NearbyFriendsFragment fragment = new NearbyFriendsFragment();
        Bundle args = new Bundle();
        //args.putString("eventList", param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

       return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_next);

        cr = getActivity().getContentResolver();
        //friendDisplayView=(TextView)getActivity().findViewById(R.id.friendDisplayView);
        PlayServiceTester playTest=new PlayServiceTester();
        if(playTest.checkPlayServices(getActivity().getApplicationContext())==true) {
            gcm = GoogleCloudMessaging.getInstance(getActivity().getApplicationContext());
            phoneNumber = getPhoneNumber(getActivity());
            Log.i("freakout", "In NextActivity "+phoneNumber);





        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("freakout","entered onCreateView");
        if (container==null) return null;
        FrameLayout mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_nearbyfriends,
                container, false);




        Log.i("freakout","inflated");




        // note that we're looking for a button with id="@+id/myButton" in your inflated layout
        // Naturally, this can be any View; it doesn't have to be a button

//        final Button refreshContactsButton = (Button) mFrameLayout.findViewById(R.id.refreshContactsButton);
//
//
//        refreshContactsButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                Log.i("freakout", "Refresh clicked");
//
//                ContactsInitializer syncGuy = new ContactsInitializer();
//                syncGuy.getContactList(cr, phonePrefix);
//                syncGuy.initializeContacts(getActivity().getApplicationContext(), phoneNumber);
//
//            }
//
//        });
//


        final CircleButton friendFinderButton = (CircleButton) mFrameLayout.findViewById(R.id.friendFinderButton);
        final EditText radiusEntryField = (EditText) mFrameLayout.findViewById(R.id.radiusEntryField);

        friendFinderButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("freakout", "Friendfinder clicked");


                String radiusInMeters = radiusEntryField.getText().toString();
                if (radiusInMeters.matches("")) {
                    Toast.makeText(getActivity(), "Please enter desired radius", Toast.LENGTH_LONG).show();
                } else {
                    PlayServiceTester playtest=new PlayServiceTester();
                    if(playtest.checkPlayServices(getActivity())==true) {
                        Bundle data = new Bundle();
                        data.putString("radius", radiusInMeters);
                        data.putString("phoneNumber", phoneNumber);
                        data.putString("url", "/user/findfriends");
                        //((TabbedActivity)getActivity()).sendMessage(data);
                        new GcmSender(data,getActivity()).execute(null, null, null);
                        Log.i("freakout","GcmSender called");
                    }
                    else Log.i("freakout","Playtest failed.");
                }
            }
        });




        Log.i("freakout", "newEventButton listener set");


        // Inflate the layout for this fragment






        return mFrameLayout;
    }


    //Function to create NearbyFriend List
    public void nearbyFriendCardList(Bundle data) {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */
        String friendList=data.getString("nearbyFriendList");
        if(friendList!=null) {
            Log.i("freakout", friendList);
            JSONArray friendListSplitted = null;
            try {
                friendListSplitted = new JSONArray(friendList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayList<Card> cards = new ArrayList<Card>();
            //Log.i("freakout",""+friendListSplitted.length);
            if (friendListSplitted.length() == 0) {
                Log.i("freakout", "Empty friend list detected");
                int errorCode = Integer.parseInt(data.get("errorCode").toString());
                switch (errorCode) {
                    case 0:
                        Toast.makeText(getActivity(), "NO nearby friends.", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(getActivity(), "Invalid phone number or user not registered", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getActivity(), "current Location not known", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(getActivity(), "NO friend List!", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else {
                for (int i = 0; i < friendListSplitted.length(); i++) {
                    // Create a Card
                    JSONObject friend = null;
                    NearbyFriendCard card = null;
                    try {
                        friend = friendListSplitted.getJSONObject(i);

                        card = new NearbyFriendCard(getActivity(), friend.getString("name"), friend.getString("phoneNumber"), friend.getString("distance"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);


                CardListView listView = (CardListView) getActivity().findViewById(R.id.nearbyFriendList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //mBroadCastReceiver = new RefreshBroadcastReceiver();
        //getActivity().registerReceiver(mBroadCastReceiver, new IntentFilter("sendData"));
    }
    private class RefreshBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("freakout","broadcast received");
            friendDisplayView.setText(intent.getExtras().get("nearbyFriendList").toString());

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // demoCard();
        nearbyFriendCardList(new Bundle());
    }
    @Override
    public void onStop() {
        super.onStop();
        // if(mBroadCastReceiver!=null)
        //     getActivity().unregisterReceiver(mBroadCastReceiver);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private String getPhoneNumber(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum = prefs.getString(PHONE_NUM, "");
        if (phNum.isEmpty()) {
            Log.i("freakout", "Phone Number not found.");
            return "";
        }
        NearbyFriendsFragment.this.phonePrefix = prefs.getString(PHONE_PREFIX, "");
        return phNum;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getActivity().getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }


}





//package com.heinz.freakoders.catchup;
//
//import android.content.BroadcastReceiver;
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.FrameLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//
///**
//* Created by sid on 1/10/15.
//*/
//public class NearbyFriendsFragment extends Fragment {
//    public static final String PHONE_PREFIX="countryCode";
//    public static final String PHONE_NUM="phoneNumber";
//    public static ContentResolver cr;
//    GoogleCloudMessaging gcm;
//    public String phonePrefix;
//    public String phoneNumber;
//    RefreshBroadcastReceiver mBroadCastReceiver;
//    public static TextView friendDisplayView;
//    // TODO: Rename parameter arguments, choose names that match
//    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String eventList;
//    //private String mParam2;
//
//    //private OnFragmentInteractionListener mListener;
//
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     *
//     * @return A new instance of fragment ExperimentFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static NearbyFriendsFragment newInstance() {
//        Log.i("freakout", "newInstance");
//        NearbyFriendsFragment fragment = new NearbyFriendsFragment();
//        Bundle args = new Bundle();
//        //args.putString("eventList", param1);
//        //args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //setContentView(R.layout.activity_next);
//
//        cr = getActivity().getContentResolver();
//        friendDisplayView=(TextView)getActivity().findViewById(R.id.friendDisplayView);
//        PlayServiceTester playTest=new PlayServiceTester();
//        if(playTest.checkPlayServices(getActivity().getApplicationContext())==true) {
//            gcm = GoogleCloudMessaging.getInstance(getActivity().getApplicationContext());
//            phoneNumber = getPhoneNumber(getActivity().getApplicationContext());
//            Log.i("freakout", "In NextActivity");
//
//
//
//
//
//        }
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        Log.i("freakout","entered onCreateView");
//        if (container==null) return null;
//        FrameLayout mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_nearbyfriends,
//                container, false);
//
//
//
//        Log.i("freakout","inflated");
//
//
//
//        // note that we're looking for a button with id="@+id/myButton" in your inflated layout
//        // Naturally, this can be any View; it doesn't have to be a button
//
//        final Button refreshContactsButton = (Button) mFrameLayout.findViewById(R.id.refreshContactsButton);
//
//
//        refreshContactsButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                Log.i("freakout", "Refresh clicked");
//
//                ContactsInitializer syncGuy = new ContactsInitializer();
//                syncGuy.getContactList(cr, phonePrefix);
//                syncGuy.initializeContacts(getActivity().getApplicationContext(), phoneNumber);
//
//            }
//
//        });
//
//
//
//        final Button friendFinderButton = (Button) mFrameLayout.findViewById(R.id.friendFinderButton);
//        final EditText radiusEntryField = (EditText) mFrameLayout.findViewById(R.id.radiusEntryField);
//
//        friendFinderButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Log.i("freakout", "Friendfinder clicked");
//
//
//                String radiusInMeters = radiusEntryField.getText().toString();
//                if (radiusInMeters.matches("")) {
//                    Toast.makeText(getActivity().getApplicationContext(), "Please enter desired radius", Toast.LENGTH_LONG).show();
//                } else {
//                    PlayServiceTester playtest=new PlayServiceTester();
//                    if(playtest.checkPlayServices(getActivity())==true) {
//                        Bundle data = new Bundle();
//                        data.putString("radius", radiusInMeters);
//                        data.putString("phoneNumber", phoneNumber);
//                        data.putString("url", "/user/findfriends");
//                        //((TabbedActivity)getActivity()).sendMessage(data);
//                        new GcmSender(data,getActivity()).execute(null, null, null);
//                        Log.i("freakout","GcmSender called");
//                    }
//                    else Log.i("freakout","Playtest failed.");
//                }
//            }
//        });
//
//
//
//        Log.i("freakout", "newEventButton listener set");
//
//
//        // Inflate the layout for this fragment
//        return mFrameLayout;
//    }
//
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        //mBroadCastReceiver = new RefreshBroadcastReceiver();
//        //getActivity().registerReceiver(mBroadCastReceiver, new IntentFilter("sendData"));
//    }
//    private class RefreshBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.i("freakout","broadcast received");
//            friendDisplayView.setText(intent.getExtras().get("nearbyFriendList").toString());
//
//        }
//    }
//    @Override
//    public void onStop() {
//        super.onStop();
//       // if(mBroadCastReceiver!=null)
//       //     getActivity().unregisterReceiver(mBroadCastReceiver);
//    }
//
//
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    private String getPhoneNumber(Context context) {
//        final SharedPreferences prefs = getGCMPreferences(context);
//        //String userName = prefs.getString(USER_NAME, "");
//        String phNum = prefs.getString(PHONE_NUM, "");
//        if (phNum.isEmpty()) {
//            Log.i("freakout", "Phone Number not found.");
//            return "";
//        }
//        NearbyFriendsFragment.this.phonePrefix = prefs.getString(PHONE_PREFIX, "");
//        return phNum;
//    }
//
//    private SharedPreferences getGCMPreferences(Context context) {
//        //This sample app persists the registration ID in shared preferences, but
//        //how you store the regID in your app is up to you.
//        return getActivity().getSharedPreferences(TabbedActivity.class.getSimpleName(),
//                Context.MODE_PRIVATE);
//    }
//
//
//}
//
