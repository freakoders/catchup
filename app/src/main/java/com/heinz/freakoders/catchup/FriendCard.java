package com.heinz.freakoders.catchup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by rahul on 28/1/15.
 */
public class FriendCard extends Card {
    /**
     * Constructor with a custom inner layout
     * @param context
     *
     */

    protected String name,number;
    protected Boolean marked;

    public FriendCard(Context context) {
        this(context, R.layout.friend_card);
    }

    public FriendCard(Context context,String name,String number) {

        this(context, R.layout.friend_card);
        this.name=name;
        this.number=number;
        this.marked=false;

        Log.d("myTag", "Friend Card constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public FriendCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

        //Set a OnClickListener listener
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
//                Toast.makeText(getContext(), "Friend card="+name, Toast.LENGTH_SHORT).show();

                marked = !(marked);
                if(marked) {
                    AddPeopleActivity.memberList+=number;
                    AddPeopleActivity.memberList+=",";

                }
                else {
                    AddPeopleActivity.memberList=AddPeopleActivity.memberList.replace(number+",","");
                }
                Log.i("freakout",AddPeopleActivity.memberList);
                //Remove click listener
                //card.setOnClickListener(null);
                //card.setClickable(false);


                //Call refresh
                card.notifyDataSetChanged();

                //Remove click listener
                //card.setOnClickListener(this);
                //card.setClickable(true);


            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");

        TextView vname = (TextView) view.findViewById(R.id.friend_name);
        TextView vnumber = (TextView) view.findViewById(R.id.friend_number);


        CircleButton callerButton=(CircleButton) view.findViewById(R.id.caller_button);
        callerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                getContext().startActivity(callIntent);

            }

        });

        vname.setText(name);

        vnumber.setText(number);
        if (marked) {

            //card.setBackgroundResourceId(R.drawable.demo_card_selector_color2);
            //card.setBackgroundColorResourceId(R.color.catchup_accent);
            ImageView img= (ImageView) view.findViewById(R.id.friend_marked);
            img.setImageResource(R.drawable.ic_tick);
            //TextView ttt=(TextView)view.findViewById(R.id.friend_number);
            //ttt.setText("xyz"+name);


        } else {
            ImageView img= (ImageView) view.findViewById(R.id.friend_marked);
            img.setImageResource(R.drawable.ic_tick_grey);



        }
    }
}
