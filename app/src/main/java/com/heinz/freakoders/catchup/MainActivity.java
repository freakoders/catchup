package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationClient;
import com.heinz.freakoders.catchup.ContactsInitializer;
import com.heinz.freakoders.catchup.GcmSender;
import com.heinz.freakoders.catchup.PlayServiceTester;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;



import static java.lang.Thread.sleep;

public class MainActivity extends FragmentActivity {

    public static double LAT,LON;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String USER_NAME = "displayName";
    public static final String PHONE_NUM="phoneNumber";
    public static final String PHONE_PREFIX="countryCode";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public ContentResolver cr;
    ContactsInitializer initializerGuy;

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */


    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCMDemo";

    TextView mDisplay;
    GoogleCloudMessaging gcm;

    SharedPreferences prefs;
    Context context;

    String regid,userName,phNum,phonePrefix;
    Intent nextIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cr=getContentResolver();
        setContentView(R.layout.activity_main);
        PlayServiceTester playTest=new PlayServiceTester();
        if(playTest.checkPlayServices(MainActivity.this)==true) {
            nextIntent=new Intent(MainActivity.this,TabbedActivity.class);
            gcm = GoogleCloudMessaging.getInstance(this);
            initializerGuy=new ContactsInitializer();
            regid = getRegistrationId(MainActivity.this);
            userName=getUserName(MainActivity.this);
            phNum=getPhNum(MainActivity.this);

            if(phNum.isEmpty()) {
                nextIntent=new Intent(MainActivity.this,VerificationActivity.class);
                //newGuyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                //newGuyIntent.putExtra("exit",true);
                //startActivity(nextIntent);


                //MainActivity.this.finish();
            }
            /*
            else {
                Log.i("freakout","PhoneNumber is "+phNum);
            }
            */


            else if(userName.isEmpty()) {
                nextIntent=new Intent(MainActivity.this,RegistrationActivity.class);
                //noNameGuyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                //noNameGuyIntent.putExtra("exit",true);
                //startActivity(noNameGuyIntent);
                //MainActivity.this.finish();
            }
            /*
            else {
                Log.i("freakout","Username is "+userName);
            }
            */

            else if (regid.isEmpty()) {
                //Toast.makeText(MainActivity.this,"Empty regid",Toast.LENGTH_LONG).show();
                //ASK FOR NAME *AND* REGISTER IN BACKGROUND.
                //registerInBackground();
                nextIntent=new Intent(MainActivity.this,InitializationActivity.class);
                nextIntent.putExtra(USER_NAME,userName);
                nextIntent.putExtra(PHONE_NUM,phNum);
                nextIntent.putExtra(PHONE_PREFIX,phonePrefix);

                /*
                Log.i("freakout","going to sleep");


                try {
                    sleep(5000);
                } catch (Exception e) {}
                Log.i("freakout","Registered now");
                */
            }

            /*
            else {
                Log.d("regid",regid);
            }
            */


            //Intent registeredIntent=new Intent(MainActivity.this,NextActivity.class);

            nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            //registeredIntent.putExtra("exit",true);
            startActivity(nextIntent);
            MainActivity.this.finish();


            //NEXT ACTIVITY IS THE ACTUAL APPLICATION ACTIVITY


            //Toast.makeText(MainActivity.this,"regid "+regid,Toast.LENGTH_LONG).show();

        }
        else {
            //if required, display error.

        }

    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private String getUserName(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String userName = prefs.getString(USER_NAME, "");
        //String phNum=prefs.getString(PHONE_NUM,"");
        if (userName.isEmpty()) {
            Log.i(TAG, "UserName not found.");
            return "";
        }
        return userName;
    }


    private String getPhNum(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        //String userName = prefs.getString(USER_NAME, "");
        String phNum=prefs.getString(PHONE_NUM,"");
        if (phNum.isEmpty()) {
            Log.i(TAG, "Phone Number not found.");
            return "";
        }
        MainActivity.this.phonePrefix=prefs.getString(PHONE_PREFIX,"");
        return phNum;
    }


    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}