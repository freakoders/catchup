package com.heinz.freakoders.catchup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import at.markushi.ui.CircleButton;


public class InitializationActivity extends ActionBarActivity {


    private AckReceiver ackReceiver;
    public static final String SENDER_ID = "233688222995";
    public static Object monitor=new Object();
    boolean selfAck=false;
    public static Boolean gcmSent=false;
    int contactsAck=0;
    public static String regid=null;

    private ProgressBar loadingSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_initialization);

        CircleButton tryAgainButton=(CircleButton) findViewById(R.id.tryAgainButton);
        ackReceiver=new AckReceiver();
        registerReceiver(ackReceiver,new IntentFilter("registeredAck"));
        registerInBackground();
        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerInBackground();
            }
        });
    }


    public void onResume() {
        super.onResume();
        ackReceiver=new AckReceiver();
        registerReceiver(ackReceiver,new IntentFilter("registeredAck"));

    }



    @Override
    public void onStop() {

        super.onStop();
        try {
            if (ackReceiver != null)
                InitializationActivity.this.unregisterReceiver(ackReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();

        try {
            if (ackReceiver != null)
                InitializationActivity.this.unregisterReceiver(ackReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
            if (ackReceiver != null)
                InitializationActivity.this.unregisterReceiver(ackReceiver);
        } catch(Exception e) {e.printStackTrace();}
    }

    private void registerInBackground() {

        final CircleButton tryAgainButton=(CircleButton) findViewById(R.id.tryAgainButton);
        tryAgainButton.setVisibility(View.GONE);
        tryAgainButton.setClickable(false);


        //SPINNER START
        loadingSpinner=(ProgressBar) findViewById(R.id.loadingSpinner);
        loadingSpinner.setVisibility(View.VISIBLE);


        new AsyncTask<Void,Void,GoogleCloudMessaging>() {

            @Override
            protected GoogleCloudMessaging doInBackground(Void... params) {
                String msg = "";
                GoogleCloudMessaging gcm=null;
                if(new PlayServiceTester().checkPlayServices(InitializationActivity.this)==true) {


                    gcm=GoogleCloudMessaging.getInstance(InitializationActivity.this);
                    try {
                        regid = gcm.register(SENDER_ID);
                        msg = "Device registered, registration ID=" + regid;

                        // You should send the registration ID to your server over HTTP,
                        // so it can use GCM/HTTP or CCS to send messages to your app.
                        // The request to your server should be authenticated if your app
                        // is using accounts.
                        gcmSent=false;
                        sendRegistrationIdToBackend(gcm,regid);
                        int i=20;          //HANG TIME IN SECONDS
//                        synchronized (monitor) {
//                            try {
//
//                                while(!gcmSent) {
//                                    Log.i("freakout", "i'm waiting...");
//                                    monitor.wait(10000);
//                                }
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
                        while(selfAck==false && i>=0) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Log.i("freakout", "waiting for self: " + i);
                            i--;
                        }


                        // For this demo: we don't need to send it because the device
                        // will send upstream messages to a server that echo back the
                        // message using the 'from' address in the message.

                        // Persist the regID - no need to register again.

                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                        // If there is an error, don't just keep trying to register.
                        // Require the user to click a button again, or perform
                        // exponential back-off.
                    }

                }
                return gcm;
            }
            @Override
            protected void onPostExecute(GoogleCloudMessaging gcm) {
                //mDisplay.append(msg + "\n");
                //Toast.makeText(MainActivity.this,msg,Toast.LENGTH_LONG).show();
                if(selfAck==true) {
                    sendContacts(gcm);

                }
                else {

                    //SPINNER STOP
                    loadingSpinner=(ProgressBar) findViewById(R.id.loadingSpinner);
                    loadingSpinner.setVisibility(View.GONE);

                    Toast.makeText(InitializationActivity.this,"Initialization failed. Please try again.",Toast.LENGTH_LONG).show();
                    tryAgainButton.setClickable(true);
                    tryAgainButton.setVisibility(View.VISIBLE);

                }


                Log.i("freakout", "contacts sent");
            }
        }.execute(null, null, null);
    }


    public void sendContacts(GoogleCloudMessaging gcmParameter) {
        final CircleButton tryAgainButton=(CircleButton) findViewById(R.id.tryAgainButton);
        final GoogleCloudMessaging gcm=gcmParameter;
        tryAgainButton.setVisibility(View.GONE);
        tryAgainButton.setClickable(false);
        new AsyncTask<Void,Void,Integer>() {

            @Override
            protected Integer doInBackground(Void... params) {
                ContactsInitializer contactsInitializer=new ContactsInitializer();
                int numContactsPackets=contactsInitializer.getContactList(InitializationActivity.this.getContentResolver(),getIntent().getStringExtra("countryCode"));
                contactsInitializer.initializeContacts(gcm,SENDER_ID,InitializationActivity.this,getIntent().getStringExtra("phoneNumber"));
                int contactsWaitTime=20;
                while (contactsAck<numContactsPackets && contactsWaitTime>=0) {
//                    synchronized (monitor) {
//                        try {
//                            monitor.wait(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
                    try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    Log.i("freakout","waiting for contacts: "+contactsWaitTime);
                    contactsWaitTime--;
                }
                return numContactsPackets;
            }

            @Override
            protected void onPostExecute(Integer numContactsPackets) {

                //SPINNER STOP
                loadingSpinner=(ProgressBar) findViewById(R.id.loadingSpinner);
                loadingSpinner.setVisibility(View.GONE);

                if(contactsAck>=numContactsPackets) {
                    new PreferencesAccesser(InitializationActivity.this).storeRegistrationId(regid,getAppVersion(InitializationActivity.this));
                    Intent successfulInitIntent=new Intent(InitializationActivity.this,TabbedActivity.class);
                    startActivity(successfulInitIntent);
                    InitializationActivity.this.finish();
                }
                else {
                    Toast.makeText(InitializationActivity.this,"Initialization failed. Please try again.",Toast.LENGTH_LONG).show();
                    tryAgainButton.setVisibility(View.VISIBLE);
                    tryAgainButton.setClickable(true);

                }
            }
        }.execute(null,null,null);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private String sendRegistrationIdToBackend(GoogleCloudMessaging gcm,String regid) {
        // Your implementation here.
        // Create a new HttpClient and Post Header
            Log.i("freakout","inside regIdToBack");
        Bundle data = getIntent().getExtras();
        //SystemClock.currentThreadTimeMillis();
        data.putString("url","/user/init/self");
        data.putString("gcmRegId",regid);
        try {
            gcm.send(SENDER_ID + "@gcm.googleapis.com", "1", data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //new GcmSender(data,InitializationActivity.this).execute(null,null,null);

        Log.i("freakout", "GCM thread created");

        return null;

    }


    private class AckReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int type=intent.getIntExtra("type",0);
            if(type==0) selfAck=true;
            else contactsAck++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_initialization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
