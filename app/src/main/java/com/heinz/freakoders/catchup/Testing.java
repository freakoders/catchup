package com.heinz.freakoders.catchup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by sid on 1/15/15.
 */
public class Testing extends Fragment {
    public static final Testing newInstance()
    {
        Testing f = new Testing();
        Bundle bdl = new Bundle(1);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.testing_layout, container, false);
        //TextView messageTextView = (TextView)v.findViewById(R.id.textView);
        Bundle data= new Bundle();
        //data.putString("hey","earth");
        new GcmSender(data,getActivity()).execute(null,null,null);

        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
