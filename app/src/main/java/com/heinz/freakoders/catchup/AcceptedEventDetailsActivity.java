package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;


public class AcceptedEventDetailsActivity extends Activity {
    String eventId;
    int flag=0;
    MembersBroadcastReceiver membersReceiver;
    EventSuggestionListBroadcastReceiver suggestReceiver;
    NewSuggestionBroadcastReceiver newSuggestReceiver;
    EtaBroadcastReceiver etaReceiver;
    JSONArray contactsArray=new JSONArray();
    ArrayList<Card> cards=new ArrayList<Card>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accepted_event_details);
        suggestReceiver=new EventSuggestionListBroadcastReceiver();
        registerReceiver(suggestReceiver,new IntentFilter("eventSuggestionListMsg"));
        eventId=getIntent().getStringExtra("id");

        newSuggestReceiver=new NewSuggestionBroadcastReceiver();
        registerReceiver(newSuggestReceiver,new IntentFilter("newSuggestionMsg"));

        etaReceiver=new EtaBroadcastReceiver();
        registerReceiver(etaReceiver,new IntentFilter("etaMsg"));
        Log.i("freakout","status is "+getIntent().getStringExtra("status"));
        if (getIntent().getStringExtra("status").equals("10")) {
            //Change layouts
            LinearLayout normalModeLayout=(LinearLayout) findViewById(R.id.normalModeLayout);
            LinearLayout etaModeLayout=(LinearLayout) findViewById(R.id.etaModeLayout);
            LinearLayout.LayoutParams suggestionParams=(LinearLayout.LayoutParams) normalModeLayout.getLayoutParams();
            suggestionParams.weight=0.0f;
            normalModeLayout.setLayoutParams(suggestionParams);
            LinearLayout.LayoutParams memberParams=(LinearLayout.LayoutParams) etaModeLayout.getLayoutParams();
            memberParams.weight=1.0f;
            etaModeLayout.setLayoutParams(memberParams);


            Bundle data=new Bundle();
            data.putString("url","/event/getetalist");
            data.putString("id",eventId);
            new GcmSender(data,AcceptedEventDetailsActivity.this).execute(null,null,null);
        }
        final CircleButton showMemberList=(CircleButton) findViewById(R.id.showMemberList);
        showMemberList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardListView suggestionView=(CardListView) findViewById(R.id.suggestionList);
                CardListView memberView=(CardListView) findViewById(R.id.acceptedEventMemberList);
                //cardView.setVisibility(View.GONE);
                if (flag==0) {
                    flag=1;
                    Bundle data=new Bundle();
                    data.putString("url", "/event/getmemberlist");
                    data.putString("id",eventId);
                    data.putString("phoneNumber", new PreferencesAccesser(AcceptedEventDetailsActivity.this).getPhNum());
                    new GcmSender(data, AcceptedEventDetailsActivity.this).execute(null, null, null);
                    CircleButton iconButton=(CircleButton) findViewById(R.id.showMemberList);
                    //iconButton.setBackground(getResources().getDrawable(R.drawable.ic_event));
                    iconButton.setImageResource(R.drawable.ic_event);
                    LinearLayout.LayoutParams suggestionParams=(LinearLayout.LayoutParams) suggestionView.getLayoutParams();
                    suggestionParams.weight=0.0f;
                    suggestionView.setLayoutParams(suggestionParams);
                    LinearLayout.LayoutParams memberParams=(LinearLayout.LayoutParams) memberView.getLayoutParams();
                    memberParams.weight=1.0f;
                    memberView.setLayoutParams(memberParams);




                }
                else {
                    flag=0;
                    Bundle sendData=new Bundle();
                    sendData.putString("id",eventId);
                    sendData.putString("url","/event/getsuggestionlist");
                    new GcmSender(sendData,AcceptedEventDetailsActivity.this).execute(null,null,null);
                    CircleButton iconButton=(CircleButton) findViewById(R.id.showMemberList);
                    iconButton.setImageResource(R.drawable.ic_nearbyfriends);
                    LinearLayout.LayoutParams suggestionParams=(LinearLayout.LayoutParams) suggestionView.getLayoutParams();
                    suggestionParams.weight=1.0f;
                    suggestionView.setLayoutParams(suggestionParams);
                    LinearLayout.LayoutParams memberParams=(LinearLayout.LayoutParams) memberView.getLayoutParams();
                    memberParams.weight=0.0f;
                    memberView.setLayoutParams(memberParams);

                }
                
                
            }
        });
        String eventname_venueString=getIntent().getStringExtra("name");
        if(!eventname_venueString.matches(""))
        {
            eventname_venueString=eventname_venueString+" @ ";

        }
        eventname_venueString=eventname_venueString+getIntent().getStringExtra("venue");
        String timedateString=getIntent().getStringExtra("time")+" on "+getIntent().getStringExtra("date");

        TextView eventname_venue=(TextView)findViewById(R.id.eventname_venue);
        eventname_venue.setText(eventname_venueString);

        TextView timedate=(TextView)findViewById(R.id.timedate);
        timedate.setText(timedateString);



        final CircleButton newSuggestionButton=(CircleButton) findViewById(R.id.newSuggestionButton);
        final Bundle data=getIntent().getExtras();

        Bundle sendData=new Bundle();
        sendData.putString("id",eventId);
        sendData.putString("url","/event/getsuggestionlist");
        new GcmSender(sendData,AcceptedEventDetailsActivity.this).execute(null,null,null);


        Bundle otherData=new Bundle();
        otherData.putString("url", "/event/getmemberlist");
        otherData.putString("id",eventId);
        otherData.putString("phoneNumber", new PreferencesAccesser(AcceptedEventDetailsActivity.this).getPhNum());
        new GcmSender(otherData, AcceptedEventDetailsActivity.this).execute(null, null, null);



        CircleButton editEventButton=(CircleButton) findViewById(R.id.editEventButton);
        editEventButton.setVisibility(View.GONE);
        editEventButton.setClickable(false);
        Log.i("freakout","creator is "+getIntent().getStringExtra("creator"));
        if(new PreferencesAccesser(AcceptedEventDetailsActivity.this).getPhNum().equals(getIntent().getStringExtra("creator"))) {
            Log.i("freakout","creator is here.");
            editEventButton.setVisibility(View.VISIBLE);
            editEventButton.setClickable(true);
            editEventButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent editEventIntent=new Intent(AcceptedEventDetailsActivity.this,AddSuggestionActivity.class);
                    editEventIntent.putExtras(data);
                    editEventIntent.putExtra("activityRole","editEvent");
                    startActivity(editEventIntent);
                }
            });
        }
        newSuggestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newSuggestionIntent=new Intent(AcceptedEventDetailsActivity.this,AddSuggestionActivity.class);
                newSuggestionIntent.putExtras(data);
                newSuggestionIntent.putExtra("activityRole","suggestion");
                startActivity(newSuggestionIntent);

            }
        });
        membersReceiver=new MembersBroadcastReceiver();
        AcceptedEventDetailsActivity.this.registerReceiver(membersReceiver, new IntentFilter("memberListMsg"));



    }


    //Function to create suggesiotn List
    public void suggestionCardList(Bundle data) {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */

        if(!(data==null)) {
            String suggestionList=data.getString("suggestionList");
            Log.i("freakout", suggestionList);
            try {
                contactsArray = new JSONArray(suggestionList);

                cards = new ArrayList<Card>();

                for (int i = 0; i <contactsArray.length(); i++) {
                    // Create a Card
                    JSONObject contact=contactsArray.getJSONObject(i);
                    SuggestionCard card = new SuggestionCard(this, contact.getString("name"), contact.getString("phoneNumber"),contact.getString("venue"),contact.getString("date"),contact.getString("time"));

                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(this, cards);


                CardListView listView = (CardListView) this.findViewById(R.id.suggestionList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            } catch (Exception e) {e.printStackTrace();}
        }

    }

    private class MembersBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout", "events Broadcast received");
            String newEventId=intent.getStringExtra("id");
            if(newEventId.equals(eventId)) {
                memberCardList(intent.getExtras());
            }

        }
    }

    private class EtaBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout", "Eta Broadcast received");
            String newEventId=intent.getStringExtra("id");
            if(newEventId.equals(eventId)) {
                etaCardList(intent.getExtras());
            }

        }
    }


    private class EventSuggestionListBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("id").equals(eventId)) {
                suggestionCardList(intent.getExtras());
            }
        }
    }

    public void memberCardList(Bundle data) {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */

        if(!(data==null)) {
            String memberList=data.getString("memberList");
            Log.i("freakout",memberList);
            try {
                JSONArray contactsArray = new JSONArray(memberList);

                ArrayList<Card> cards = new ArrayList<Card>();

                for (int i = 0; i <contactsArray.length(); i++) {
                    // Create a Card
                    JSONObject contact=contactsArray.getJSONObject(i);
                    EventMemberCard card = new EventMemberCard(this, contact.getString("name"), contact.getString("phoneNumber"),Integer.parseInt(contact.getString("status")));

                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(this, cards);


                CardListView listView = (CardListView) this.findViewById(R.id.acceptedEventMemberList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            } catch (Exception e) {e.printStackTrace();}
        }

    }

    public void etaCardList(Bundle data) {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */

        if(!(data==null)) {
            String memberList=data.getString("etaList");
            Log.i("freakout",memberList);
            try {
                JSONArray contactsArray = new JSONArray(memberList);

                ArrayList<Card> cards = new ArrayList<Card>();

                for (int i = 0; i <contactsArray.length(); i++) {
                    // Create a Card
                    JSONObject contact=contactsArray.getJSONObject(i);
                    EtaCard card = new EtaCard(this, contact.getString("name"), contact.getString("phoneNumber"),contact.getString("etaTime"),contact.getString("etaDistance"));

                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(this, cards);


                CardListView listView = (CardListView) this.findViewById(R.id.etaEventMemberList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            } catch (Exception e) {e.printStackTrace();}
        }

    }

    private class NewSuggestionBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.i("freakout","new suggestions broadcast received");
                JSONObject suggestion = new JSONObject(intent.getStringExtra("suggestion"));
                SuggestionCard card = new SuggestionCard(AcceptedEventDetailsActivity.this, suggestion.getString("name"), suggestion.getString("phoneNumber"),suggestion.getString("venue"),suggestion.getString("date"),suggestion.getString("time"));
                cards.add(card);
            CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(AcceptedEventDetailsActivity.this, cards);

        String eventname_venueString=getIntent().getStringExtra("name");
        if(!eventname_venueString.matches(""))
        {
            eventname_venueString=eventname_venueString+" @ ";

        }
        eventname_venueString=eventname_venueString+getIntent().getStringExtra("venue");
        String timedateString=getIntent().getStringExtra("time")+" on "+getIntent().getStringExtra("date");

        TextView eventname_venue=(TextView)findViewById(R.id.eventname_venue);
        eventname_venue.setText(eventname_venueString);

        TextView timedate=(TextView)findViewById(R.id.timedate);
        timedate.setText(timedateString);



            CardListView listView = (CardListView) AcceptedEventDetailsActivity.this.findViewById(R.id.suggestionList);

            if (listView != null) {
                listView.setAdapter(mCardArrayAdapter);


            }
            } catch (Exception e) {e.printStackTrace();}
        }
    }



    @Override
    public void onResume() {
        super.onResume();

        suggestReceiver=new EventSuggestionListBroadcastReceiver();
        registerReceiver(suggestReceiver, new IntentFilter("eventSuggestionListMsg"));

        newSuggestReceiver=new NewSuggestionBroadcastReceiver();
        registerReceiver(newSuggestReceiver, new IntentFilter("newSuggestionMsg"));

        membersReceiver=new MembersBroadcastReceiver();
        AcceptedEventDetailsActivity.this.registerReceiver(membersReceiver, new IntentFilter("memberListMsg"));

        etaReceiver=new EtaBroadcastReceiver();
        AcceptedEventDetailsActivity.this.registerReceiver(etaReceiver, new IntentFilter("etaMsg"));
    }




    @Override
    public void onStop() {

        super.onStop();
        try {
            if (suggestReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(suggestReceiver);

            }
            if (newSuggestReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(newSuggestReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (membersReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout", "membersreceiver unregistered.");
            }

            if (etaReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(etaReceiver);

            }
        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();
        try {
            if (suggestReceiver != null){
                Log.i("freakout","membersreceiver unregistered.");
                AcceptedEventDetailsActivity.this.unregisterReceiver(suggestReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (newSuggestReceiver != null){
                Log.i("freakout","membersreceiver unregistered.");
                AcceptedEventDetailsActivity.this.unregisterReceiver(newSuggestReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }

            if (membersReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (etaReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(etaReceiver);

            }
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
            if (suggestReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(suggestReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (newSuggestReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(newSuggestReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (membersReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
            if (etaReceiver != null) {
                AcceptedEventDetailsActivity.this.unregisterReceiver(etaReceiver);

            }
        } catch(Exception e) {e.printStackTrace();}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accepted_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
