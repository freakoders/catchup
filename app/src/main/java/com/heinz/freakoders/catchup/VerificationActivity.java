package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.heinz.freakoders.catchup.MainActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;


import at.markushi.ui.CircleButton;

import static android.R.color.background_light;
import static java.lang.Thread.sleep;



public class VerificationActivity extends Activity {

    String phoneNo,sid,mOTP,phonePrefix;
    private ProgressBar verifyingSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verification);

        final CircleButton verifyButton= (CircleButton) findViewById(R.id.verifyButton);
        verifyingSpinner=(ProgressBar) findViewById(R.id.loadingSpinner);
        verifyingSpinner.setVisibility(View.GONE);



        verifyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                verifyButton.setClickable(false);
                verifyButton.setVisibility(View.GONE);
                //verifyButton.setTextColor(background_light);

                EditText phone=(EditText) findViewById(R.id.phoneField);
                phoneNo=phone.getText().toString();
                EditText phonePrefixEntry=(EditText) findViewById(R.id.phonePrefixField);
                phonePrefix=phonePrefixEntry.getText().toString();
                phonePrefix="+" + phonePrefix;
                phoneNo=phonePrefix+phoneNo;
                Toast.makeText(VerificationActivity.this, phoneNo, Toast.LENGTH_LONG).show();
                verifyingSpinner.setVisibility(View.VISIBLE);
                new resp().execute();
            }
        });
    }

    public class resp extends AsyncTask<Void, Void, Void> {

        public String getSid() throws IOException {

            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("https://motp.p.mashape.com/v1/0004-75c36b2d-53bf85ed-9d2e-630840b7/"+phoneNo);
            request.addHeader("X-Mashape-Key","3Ce3DMPpvwmshK2yyvg3KaVSTzJtp1wNh9Zjsng4gdilKaQdp9");
            //OLD : api.motp.in ;remove header
            //X mashape key for app : CatchUpTest on Rahul's github
            //response.addHeader("X-Mashape-Key","<required>");
            String result = null,status=null;
            try {
                HttpResponse response= client.execute(request);
                HttpEntity entity = response.getEntity();

                String json_string = EntityUtils.toString(entity);

                try {
                    JSONObject temp1 = new JSONObject(json_string);
                    status=temp1.getString("Status");
                    if(status.equalsIgnoreCase("Success")){
                        result= temp1.getString("Result");
                        return result;
                    }
                    else{
                        throw new IOException();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

		    /*
		        if (entity != null) {

		            // A Simple JSON Response Read
		            InputStream instream = entity.getContent();
		            BufferedReader reader = new BufferedReader(new InputStreamReader(instream));

		           reader.readLine();
		           reader.readLine();

		           return reader.readLine();

		        }

		        else
		        	return "HI!!";
		       */
            } catch (ClientProtocolException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();


            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            return null;

        }

        public String getMOTP(String sid) throws IOException {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            //HttpPost httppost = new HttpPost("https://api.mOTP.in/v1/OTP/0004-75c36b2d-53bf85ed-9d2e-630840b7/"+sid);
            HttpPost httppost = new HttpPost("https://motp.p.mashape.com/v1/OTP/0004-75c36b2d-53bf85ed-9d2e-630840b7/"+sid);
            httppost.addHeader("X-Mashape-Key","3Ce3DMPpvwmshK2yyvg3KaVSTzJtp1wNh9Zjsng4gdilKaQdp9");
            String status=null,result=null;
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("private", "0009-75c36b2d-53bf85ed-9d32-36ffce6e"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                int stat=response.getStatusLine().getStatusCode();
                String json_string = EntityUtils.toString(entity);
                Log.e("freak",Integer.toString(stat));
                try {
                    JSONObject temp1 = new JSONObject(json_string);
                    status=temp1.getString("Status");
                    if(status.equalsIgnoreCase("Success")){
                        result= temp1.getString("Result");
                        return result;
                    }
                    else{
                        throw new IOException();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
            return null;

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

            try {
                sid = getSid();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                mOTP = getMOTP(sid);
                mOTP="+"+mOTP;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            new AsyncTask<Void, Void, Integer>() {
                @Override
                protected Integer doInBackground(Void... params) {
                    ContentResolver cr;
                    Cursor cur;
                    //Toast.makeText(VerificationActivity.this, sid, Toast.LENGTH_LONG).show();
                    //Toast.makeText(VerificationActivity.this, mOTP, Toast.LENGTH_LONG).show();
                    int i = 10;
                    int flag = 0;
                    while (i > 0) {
                        try {
                            sleep(1000);
                        } catch (Exception e) {

                        }
                        cr = getContentResolver();
                        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";
                        //Uri callUri = Uri.parse("content://call_log/calls");
                        //Cursor cur=cr.query(uri, projection, selection, selectionArgs, sortOrder)

                        cur = cr.query(CallLog.Calls.CONTENT_URI, null, null, null, strOrder);
                        // loop through cursor

                        if (cur.moveToFirst() == true) {

                            String lastCall = cur.getString(cur.getColumnIndex("number"));
                            //String lastCall=cur.getColumnName(5);
                            //CallLog.Calls.getLastOutgoingCall(getApplicationContext());
                            //Toast.makeText(getApplicationContext(), "" + lastCall, Toast.LENGTH_SHORT).show();
                            if (lastCall.equals(mOTP)) {
                                cur.close();
                                flag = 1;
                                break;

                            } else {
                                //Toast.makeText(getApplicationContext(), "FAIL", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //Toast.makeText(getApplicationContext(), "FAIL", Toast.LENGTH_LONG).show();
                        }
                        i--;
                    }
                    return (Integer) flag;

                }

                @Override
                protected void onPostExecute(Integer flag) {
                    CircleButton verifyButton = (CircleButton) findViewById(R.id.verifyButton);
                    if (flag == 1) {
                        Toast.makeText(getApplicationContext(), "Verified!", Toast.LENGTH_SHORT).show();
                        storePhoneNum(VerificationActivity.this, phoneNo);
                        Intent verifiedIntent = new Intent(VerificationActivity.this, RegistrationActivity.class);
                        startActivity(verifiedIntent);
                        verifiedIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        VerificationActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "FAIL", Toast.LENGTH_LONG).show();

                    }
                    verifyingSpinner.setVisibility(View.GONE);
                    verifyButton.setClickable(true);
                    verifyButton.setVisibility(View.VISIBLE);
                    //verifyButton.setTextColor(Color.BLACK);
                }


            }.execute(null, null, null);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void storePhoneNum(Context context, String phNum) {
        final SharedPreferences prefs = getGCMPreferences(context);
        Log.i("freakout", "Saving phone number ");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MainActivity.PHONE_NUM, phNum);
        editor.putString(MainActivity.PHONE_PREFIX,phonePrefix);
        editor.commit();
    }


}