package com.heinz.freakoders.catchup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by rahul on 28/1/15.
 */
public class NearbyFriendCard extends Card{

    /**
     * Constructor with a custom inner layout
     * @param context
     *
     */

    protected String name,number,distance;

    public NearbyFriendCard(Context context) {
        this(context, R.layout.nearbyfriend_card);
    }

    public NearbyFriendCard(Context context,String name,String number,String distance) {

        this(context, R.layout.nearbyfriend_card);
        this.name=name;
        this.number=number;
        this.distance=distance;

        Log.d("myTag", "NearbyFriend Card constructor");
    }



    /**
     *
     * @param context
     * @param innerLayout
     */
    public NearbyFriendCard(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
        Log.d("myTag", "Init called");
    }



    /**
     * Init
     */
    private void init(){

        //No Header

        //Set a OnClickListener listener
        setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                Toast.makeText(getContext(), "Nearby Friend Card="+number, Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        Log.i("myTag","setupInnerView");

        TextView vname = (TextView) view.findViewById(R.id.nearbyfriend_name);
        TextView vnumber = (TextView) view.findViewById(R.id.nearbyfriend_number);
        TextView vdistance = (TextView) view.findViewById(R.id.nearbyfriend_distance);
        CircleButton callerButton=(CircleButton) view.findViewById(R.id.caller_button);
        callerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                getContext().startActivity(callIntent);

            }

        });

        vname.setText(name);
        vnumber.setText(number);
        vdistance.setText(distance);

    }
}
