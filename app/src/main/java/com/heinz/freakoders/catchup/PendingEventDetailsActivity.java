package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;


public class PendingEventDetailsActivity extends Activity {


    /*
    TODO: 1. Make a cardList for members 2. Make a BroadCastReceiver to get memberList from Server 3. Add accept/reject options.
     */
    String eventId;
    MembersBroadcastReceiver membersReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_event_details);
        eventId=getIntent().getStringExtra("id");

        String eventname_venueString=getIntent().getStringExtra("name");
        if(!eventname_venueString.matches(""))
        {
            eventname_venueString=eventname_venueString+" @ ";

        }
        eventname_venueString=eventname_venueString+getIntent().getStringExtra("venue");
        String timedateString=getIntent().getStringExtra("time")+" on "+getIntent().getStringExtra("date");

        TextView eventname_venue=(TextView)findViewById(R.id.eventname_venue);
        eventname_venue.setText(eventname_venueString);

        TextView timedate=(TextView)findViewById(R.id.timedate);
        timedate.setText(timedateString);

        final CircleButton acceptEventButton=(CircleButton) findViewById(R.id.acceptEventButton);
        acceptEventButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "Event accepted");

                    Bundle data=new Bundle();
                    data.putString("phoneNumber", new PreferencesAccesser(PendingEventDetailsActivity.this).getPhNum());
                    data.putString("id",eventId);
                    data.putString("url", "/event/acceptevent");
                    //Intent eventCreatedIntent = new Intent(AddPeopleActivity.this, EventsActivity.class);
                    new GcmSender(data,PendingEventDetailsActivity.this).execute(null,null,null);
                    //startActivity(eventCreatedIntent);
                    Intent acceptedEventIntent=new Intent(PendingEventDetailsActivity.this,AcceptedEventDetailsActivity.class);
                    acceptedEventIntent.putExtras(getIntent().getExtras());
                    acceptedEventIntent.putExtra("status","1");
                    startActivity(acceptedEventIntent);
                    PendingEventDetailsActivity.this.finish();

                }


        });

        final CircleButton rejectEventButton=(CircleButton) findViewById(R.id.rejectEventButton);
        rejectEventButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Log.i("freakout", "Event rejected");

                Bundle data=new Bundle();
                data.putString("phoneNumber", new PreferencesAccesser(PendingEventDetailsActivity.this).getPhNum());
                data.putString("id",eventId);
                data.putString("url", "/event/rejectevent");

                new GcmSender(data,PendingEventDetailsActivity.this).execute(null,null,null);

                PendingEventDetailsActivity.this.finish();

            }


        });


        Bundle dummyBundle=null;
        memberCardList(dummyBundle);
        membersReceiver=new MembersBroadcastReceiver();
        PendingEventDetailsActivity.this.registerReceiver(membersReceiver, new IntentFilter("memberListMsg"));
        Bundle data = new Bundle();
        data.putString("url", "/event/getmemberlist");
        data.putString("id",eventId);
        data.putString("phoneNumber", new PreferencesAccesser(PendingEventDetailsActivity.this).getPhNum());
        new GcmSender(data, PendingEventDetailsActivity.this).execute(null, null, null);
    }

    private class MembersBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            Log.i("freakout", "events Broadcast received");
            String newEventId=intent.getStringExtra("id");
            if(newEventId.equals(eventId)) {
                memberCardList(intent.getExtras());
            }

        }
    }





    //Function to create Contact List
    public void memberCardList(Bundle data) {
        /*
        HERE I NEED TO EDIT THE FOR LOOP AFTER JSONIFYING THE BUNDLE AND PASSING DATA FROM ACTIVITY TO FRAGMENT
         */

        if(!(data==null)) {
            String memberList=data.getString("memberList");
            Log.i("freakout",memberList);
            try {
                JSONArray contactsArray = new JSONArray(memberList);

                ArrayList<Card> cards = new ArrayList<Card>();

                for (int i = 0; i <contactsArray.length(); i++) {
                    // Create a Card
                    JSONObject contact=contactsArray.getJSONObject(i);
                    EventMemberCard card = new EventMemberCard(this, contact.getString("name"), contact.getString("phoneNumber"),Integer.parseInt(contact.getString("status")));

                    // Create a CardHeaders
                    //CardHeader header = new CardHeader(this);
                    //header.setTitle("+9011020760");
                    // Add Header to card
                    //card.addCardHeader(header);


                    //CardThumbnail thumb = new CardThumbnail(getActivity());
                    //thumb.setDrawableResource(R.drawable.ic_category_food);
                    //card.addCardThumbnail(thumb);

                    cards.add(card);
                }

                CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(this, cards);


                CardListView listView = (CardListView) this.findViewById(R.id.memberList);

                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);


                }
            } catch (Exception e) {e.printStackTrace();}
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        membersReceiver=new MembersBroadcastReceiver();
        PendingEventDetailsActivity.this.registerReceiver(membersReceiver, new IntentFilter("memberListMsg"));

    }




    @Override
    public void onStop() {

        super.onStop();
        try {
            if (membersReceiver != null) {
                PendingEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }
    @Override
    public  void onDestroy() {
        super.onDestroy();
        try {
            if (membersReceiver != null) {
                PendingEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public  void onPause() {
        super.onPause();
        try {
            if (membersReceiver != null) {
                PendingEventDetailsActivity.this.unregisterReceiver(membersReceiver);
                Log.i("freakout","membersreceiver unregistered.");
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pending_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
