package com.heinz.freakoders.catchup;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by sid on 8/30/14.
 */
public class PlayServiceTester {

    public PlayServiceTester() {}

    public boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity)context,0).show();
            }
            else {
                Toast.makeText(context,"Could not connect to PlayServices. Please try again.",Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }
}
